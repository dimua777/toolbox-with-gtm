/*
  Common configs for all packages
*/
module.exports = {
  babelrcRoots: ['app/*', 'ui/*'],
  plugins: [],
  presets: ['next/babel'],
};
