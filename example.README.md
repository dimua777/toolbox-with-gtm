# FIXME: {projectName}

FIXME: provide some project information

## Available Scripts

- `yarn dev` - run main app in dev mode
- `yarn build` - build main app
- `yarn start` - build main app in dev mode
- `yarn extract` - extract theme from figma into `ui`
- `yarn g` - generate new feature/component/lib etc using `plop`
- `yarn lint` - lint files using `eslint`
- `yarn test` - run tests
- `yarn sb` - run `storybook` in dev mode
- `yarn sb:build` - build `storybook`

> some other useful commands can be found directly in packages

## Requirements

- Node.js lts+, Yarn (use [Volta](https://volta.sh/) for manage versions)
- Use `dev` npm script instead `start` for development
- Use kebab-case file naming
- Every package should has acceptable readme

## Commitizen

`git-cz` is recommended

## Updating dependencies

`yarn upgrade-interactive` or `yarn upgrade-interactive --latest` (for updating to latest packages versions)
