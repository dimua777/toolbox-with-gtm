# FIXME: {projectName} UI-kit

reusable ui components

### Roadmap:

- [x] FieldError
- [x] Icon
- [x] Skeleton
- [x] Spinner
- [x] Button
- [x] FormField (final-form wrapper)
- [x] PasswordField
- [x] TextField (input/textarea)
- [ ] MaskedTextField
- [ ] SelectField
- [ ] MultiSelectField
- [ ] DateField/DateTimeField/TimeField
- [ ] Modal
- [ ] PageModal
- [ ] ConfirmModal
- [ ] Notification/Snack
- [ ] Alert
- [ ] VideoPlayer (with cgma like features support)
- [ ] RichEditor
