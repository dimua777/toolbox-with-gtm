import { useEffect } from 'react';
import { useWindowSize } from 'react-use';

export const ViewPortHeightUpdater = () => {
  const { height } = useWindowSize();

  useEffect(() => {
    if (typeof window === 'object') {
      document.documentElement.style.setProperty('--vh', height / 100 + 'px');
    }
  }, [height]);

  return null;
};
