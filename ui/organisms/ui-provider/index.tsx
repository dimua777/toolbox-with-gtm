import { FC } from 'react';

import { ViewPortHeightUpdater } from '../viewport-height-updater';

// TODO: when providers will be ready add more here (ModalProvider etc)
// FIXME: remove things that don't fit you project
export const UIProvider: FC = ({ children }) => (
  <>
    {children}
    <ViewPortHeightUpdater />
  </>
);
