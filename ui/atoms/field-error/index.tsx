import { FC } from 'react';
import { css } from 'astroturf';

export type FieldErrorProps = { className?: string };

export const FieldError: FC<FieldErrorProps> = ({ children, className = '' }) => {
  return (
    <span
      className={`text-error100 border-error100 bg-bg100 v-bs-input-error100 absolute bottom-0 right-1 block -mb-1 px-1 border rounded-lg ${className}`}
      css={css`
        &::first-letter {
          text-transform: uppercase;
        }
      `}
    >
      {children}
    </span>
  );
};
