import styled from 'astroturf/react';

import primary from './primary.svg';
import primaryShort from './primary-short.svg';

export type LogoProps = { variant?: 'primary' | 'primary-short'; className?: string };

const logoProps = {
  primary: {
    height: 72,
    src: primary,
    width: 248,
  },
  'primary-short': {
    height: 72,
    src: primaryShort,
    width: 72,
  },
};

const LogoStyled = styled.img`
  @apply flex-shrink-0;
`;

export const Logo = ({
  variant = 'primary',
  ...rest
}: LogoProps & GetComponentProps<typeof LogoStyled>) => {
  const extra = logoProps[variant];
  return <LogoStyled alt="FIXME: {projectName} logo" {...extra} {...rest} />;
};
