export type IconsType = 'visibility-on' | 'visibility-off' | 'clear' | 'check';
export const ICONS = ['visibility-on', 'visibility-off', 'clear', 'check'];
