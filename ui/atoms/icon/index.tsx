import { FC, memo } from 'react';
import styled from 'astroturf/react';

import icons from './sprite.svg';
import { IconsType } from './types';

type BasicProps = { size?: number; width?: number; height?: number; [key: string]: any };

export type IconProps =
  | ({
      name: string;
      src: string;
      type: 'custom';
    } & BasicProps)
  | ({
      name: IconsType;
    } & BasicProps);

export const StyledSvg = styled.svg`
  @apply fill-current inline-flex flex-shrink-0;
`;

const IconImpl = ({
  size = 24,
  width,
  height,
  src = icons,
  name,
  ...rest
}: IconProps & GetComponentProps<typeof StyledSvg>) => (
  <StyledSvg
    aria-hidden="true"
    focusable="false"
    role="presentation"
    style={{ width: width || size, height: height || size }}
    {...rest}
  >
    <use xlinkHref={`${src}#${name}`} />
  </StyledSvg>
);

export const Icon = memo(IconImpl);

// TODO: fix in future if `ArgsTable` start support conditional types
// Sorry for this, but StoryBook is stupid
export const SBIcon = (() => null as unknown) as FC<
  {
    name: IconsType;
  } & BasicProps
>;

export const SBCustomIcon = (() => null as unknown) as FC<
  {
    name: string;
    src: string;
    type: 'custom';
  } & BasicProps
>;
