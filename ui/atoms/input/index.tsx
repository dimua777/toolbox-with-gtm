import styled from 'astroturf/react';

type InputProps = {
  invalid?: boolean;
  // TODO: astroturf won't work with size. try again later 🤣
  size?: 'sm' | 'lg';
};

export const Input = styled.input<InputProps>`
  @apply appearance-none w-full px-12px border border-outline300 bg-bg200 rounded-lg text-txt900 transition-colors duration-300;

  &:focus {
    @apply border-outline400 outline-none ring-1 ring-outline400;
  }

  &:disabled {
    @apply border-outline100 text-txt400;
  }

  &.invalid {
    @apply border-error100;

    &:focus {
      @apply ring-0 border-error100;
    }
  }

  &.size-sm {
    @apply v-bs-input-val100;
    height: 32px;

    /* iOS Safari Hack */
    &[type='date'] {
      min-height: 32px;
    }

    &[type='time'] {
      min-height: 32px;
    }
  }

  &.size-lg {
    @apply v-bs-input-val200;
    height: 40px;

    /* iOS Safari Hack */
    &[type='date'] {
      min-height: 40px;
    }

    &[type='time'] {
      min-height: 40px;
    }
  }

  textarea& {
    @apply py-2 resize-none;
  }
`;
