import { FC, useMemo } from 'react';
import styled from 'astroturf/react';

export type SkeletonProps = {
  count?: number;
  className?: string;
  circle?: boolean;
};

const SkeletonStyled = styled.span`
  @apply animate-pulse bg-outline300 inline-flex w-full;
  line-height: 1;

  &::before {
    content: '\00a0';
  }
`;

export const Skeleton: FC<SkeletonProps> = ({
  count = 1,
  circle = false,
  className = '',
  ...rest
}) => {
  const elements = useMemo(() => new Array(count).fill(1), [count]);

  return (
    <>
      {elements.map((_, index) => (
        <SkeletonStyled
          key={index}
          {...rest}
          className={`animate-pulse ${circle ? ' rounded-full' : 'rounded-sm'} ${className}`}
        />
      ))}
    </>
  );
};
