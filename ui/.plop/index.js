/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const distDirectory = path.join(__dirname, '../');
const templateDirectory = path.join(__dirname, './');

module.exports = (plop) => {
  plop.setGenerator('component', {
    description: 'Create a new component',
    prompts: [
      {
        type: 'list',
        name: 'taxon',
        choices: ['atoms', 'molecules', 'organisms'],
        message: 'What is component type?',
      },
      {
        type: 'input',
        name: 'component',
        message: 'What is your component name (will be transformed to kebab-case)?',
      },
    ],
    actions: [
      {
        type: 'add',
        path: `${distDirectory}/{{taxon}}/{{kebabCase component}}/index.tsx`,
        templateFile: `${templateDirectory}/index.plop`,
      },
      {
        type: 'add',
        path: `${distDirectory}/{{taxon}}/{{kebabCase component}}/{{kebabCase component}}.stories.mdx`,
        templateFile: `${templateDirectory}/stories.plop`,
      },
      {
        type: 'append',
        path: `${distDirectory}/{{taxon}}/index.ts`,
        separator: '',
        template: `export * from './{{kebabCase component}}';\n`,
      },
    ],
  });
};
