function getKeyName(name) {
  // format name from like "Heading/bs-h200 - 80 b" to "bs-h200"

  const splitLeftPart = name?.split('/');
  const splitRightPart = splitLeftPart?.[splitLeftPart?.length - 1]
    .split(' ')[0]
    ?.replace('.', '-');

  return splitRightPart || '';
}

const iconNaming = originalName => {
  const formattedName = originalName.replace(/ /g, '').replace('/', '-');
  return formattedName;
};

module.exports = {
  apiKey: '165157-cae1b772-1c6c-4ddb-b299-40027e3a6036',
  fileId: '7NnFCu4jFGBM1EIXSWNAVX',
  styles: {
    exportPath: './theme',
    colors: {
      keyName: getKeyName,
    },
    effects: {},
    textStyles: {
      keyName: nameFromFigma => `.v-${getKeyName(nameFromFigma)}`,
    },
    gradients: {
      disabled: true,
    },
  },
  icons: {
    nodeIds: ['1029:228'],
    iconName: name => iconNaming(name), // custom format icon name
    exportPath: './atoms/icon',
    generateSprite: true,
    generateTypes: true,
  },
};
