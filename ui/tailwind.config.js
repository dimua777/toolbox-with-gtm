/* eslint-disable @typescript-eslint/no-var-requires */
const plugin = require('tailwindcss/plugin');

const colors = require('./theme/colors');
const { boxShadow } = require('./theme/effects');
const { textVariants, fontFamily } = require('./theme/text-styles');

module.exports = {
  mode: 'jit',
  purge: ['./**/*.{js,ts,jsx,tsx}'],
  plugins: [
    require('@tailwindcss/aspect-ratio'),
    require('@tailwindcss/line-clamp'),
    plugin(function ({ addUtilities, addVariant, e }) {
      addUtilities(textVariants, { variants: ['responsive'], respectImportant: false });

      // Support for: empty:hidden, xs:empty:flex etc
      addVariant('empty', ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) => {
          return `.${e(`empty${separator}${className}`)}:empty`;
        });
      });
    }),
  ],
  important: true,
  theme: {
    ...require('./theme/spacing'),
    screens: {
      sm: '768px',
      md: '1024px',
      lg: '1440px',
      xl: '1776px',
    },
    container: {
      center: true,
      padding: {
        DEFAULT: '16px',
        sm: '24px',
        md: '32px',
        lg: '48px',
      },
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      ...colors,
    },
    fontSize: {},
    boxShadow,
    fontFamily,
  },
};
