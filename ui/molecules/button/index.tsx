import { FC, forwardRef } from 'react';
import styled from 'astroturf/react';

type ButtonVariant = 'primary' | 'secondary';
type ButtonSize = 'sm' | 'lg';

type StyledButtonProps = {
  variant?: ButtonVariant;
  size?: ButtonSize;
  rounded?: boolean;
  loading?: boolean;
};

import { Spinner, StyledSpinner } from '../../atoms/spinner';

export const StyledButton = styled.button<StyledButtonProps>`
  @apply transition delay-150 appearance-none inline-flex items-center justify-center;
  @apply v-bs-btn100 px-3 text-txt100 rounded-lg relative;

  &:focus {
    @apply outline-none ring-4 ring-outline400;
  }

  &:disabled {
    @apply cursor-auto;
  }

  &.variant-primary {
    @apply bg-accent900;

    &:hover {
      @apply bg-accent800;
    }

    &:focus {
      @apply bg-accent900;
    }

    &:disabled:not(.loading) {
      @apply bg-accent200 text-txt300;
    }
  }

  &.variant-secondary {
    @apply bg-transparent border-2 border-accent900 text-accent900;

    &:hover {
      @apply border-accent800;
    }

    &:focus {
      @apply border-accent900;
    }

    &:disabled:not(.loading) {
      @apply border-accent200 text-txt300;
    }

    &.loading ${StyledSpinner} {
      @apply text-accent900;
    }
  }

  &.size-lg {
    height: 40px;
  }

  &.size-sm {
    height: 32px;
  }

  &.rounded {
    @apply rounded-full;
  }

  &.loading {
    @apply text-transparent;
  }

  & ${StyledSpinner} {
    @apply absolute left-1/2 self-center transform -translate-x-1/2 text-txt100;
  }
`;

export const Button = forwardRef(
  (
    {
      children,
      rounded = false,
      size = 'lg',
      variant = 'primary',
      type,
      disabled,
      loading,
      ...rest
    },
    ref,
  ) => {
    const resultType = !rest.as && !type ? 'button' : type;
    const resultDisabled = disabled || loading ? true : disabled;

    return (
      <StyledButton
        ref={ref}
        disabled={resultDisabled}
        loading={loading}
        rounded={rounded}
        size={size}
        type={resultType}
        variant={variant}
        {...rest}
      >
        {children}
        {loading && <Spinner size={size === 'lg' ? 24 : 16} />}
      </StyledButton>
    );
  },
) as FC<GetAstroturfComponentProps<typeof StyledButton>>;
