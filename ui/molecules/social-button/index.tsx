import styled from 'astroturf/react';

import appleIcon from './icons/apple.svg';
import fbIcon from './icons/fb.svg';
import googleIcon from './icons/google.svg';

type Provider = 'apple' | 'fb' | 'google';

type StyledButtonProps = {
  provider: Provider;
};

const iconByProvider: { [Property in Provider]: any } = {
  apple: appleIcon,
  fb: fbIcon,
  google: googleIcon,
};

const StyledButton = styled.button<StyledButtonProps>`
  @apply transition appearance-none gap-1 inline-flex items-center;
  @apply v-bs-btn200 px-1;
  height: 40px;
  border-radius: 10px;

  &:focus {
    @apply outline-none ring-4 ring-outline400;
  }

  &:hover {
    @apply opacity-70;
  }

  &:disabled {
    @apply cursor-auto opacity-20;
  }

  &.provider-apple {
    background-color: #000;
    color: #fff;
  }

  &.provider-fb {
    background-color: #1877f2;
    color: #fff;
  }

  &.provider-google {
    background: #ffffff;
    box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.084), 0px 2px 3px rgba(0, 0, 0, 0.168);
    color: rgba(0, 0, 0, 0.54);

    &:focus {
      box-shadow: 0px 0px 9px rgba(0, 0, 0, 0.084), 0px 5px 9px rgba(0, 0, 0, 0.168);
    }
  }
`;

export const SocialButton = ({
  children,
  provider,
  type,
  ...rest
}: GetAstroturfComponentProps<typeof StyledButton>) => {
  const resultType = !rest.as && !type ? 'button' : type;

  return (
    <StyledButton provider={provider} type={resultType} {...rest}>
      <img alt={`${provider} icon`} src={iconByProvider[provider]} />
      <span className="flex-grow text-center">{children}</span>
    </StyledButton>
  );
};
