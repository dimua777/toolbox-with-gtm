import { FC, ReactNode } from 'react';

import { FieldError, Input } from '../../atoms';

export type TextFieldProps = {
  autoComplete?: string;
  error?: string;
  label?: ReactNode;
  name?: string;
  onChange?: (value: string) => void;
  placeholder?: string;
  size?: 'sm' | 'lg';
  type?: HTMLInputElement['type'];
  value?: string;
};

export const TextField: FC<TextFieldProps> = ({
  name,
  value,
  onChange,
  error,
  label,
  size = 'lg',
  children,
  ...rest
}) => {
  return (
    <label className="inline-flex flex-col appearance-none">
      {label && <div className="v-bs-input-label200 mb-4px">{label}</div>}
      <div className="relative inline-flex">
        <Input
          aria-invalid={!!error}
          autoCapitalize="off"
          autoCorrect="off"
          invalid={!!error}
          name={name}
          size={size as any}
          value={value}
          onChange={onChange as any}
          {...rest}
        />
        {children}
        {error && <FieldError>{error}</FieldError>}
      </div>
    </label>
  );
};
