import { FC, forwardRef } from 'react';
import styled from 'astroturf/react';

type ButtonVariant = 'primary';
type ButtonSize = 'sm' | 'lg';

type StyledButtonProps = {
  variant?: ButtonVariant;
  size?: ButtonSize;
};

const StyledButton = styled.button<StyledButtonProps>`
  @apply transition delay-150 appearance-none inline-flex items-center justify-center;
  @apply v-bs-btn200;

  &:focus {
    @apply outline-none ring-4 ring-outline400;
  }

  &:disabled {
    @apply cursor-auto;
  }

  &.variant-primary {
    @apply text-txt700;

    &:hover {
      @apply text-accent900;
    }

    &:disabled {
      @apply text-txt300;
    }
  }

  &.size-lg {
    height: 24px;
  }

  &.size-sm {
    @apply v-bs-btn100;
    height: 16px;
  }
`;

export const TextButton = forwardRef(
  ({ children, size = 'lg', variant = 'primary', type, ...rest }, ref) => {
    const resultType = !rest.as && !type ? 'button' : type;

    return (
      <StyledButton ref={ref} size={size} type={resultType} variant={variant} {...rest}>
        {children}
      </StyledButton>
    );
  },
) as FC<GetAstroturfComponentProps<typeof StyledButton>>;
