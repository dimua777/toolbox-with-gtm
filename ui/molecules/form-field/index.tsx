import { FC, useMemo } from 'react';
import { useField } from 'react-final-form';

type FormFieldProps<T = FC, RC = FC> = {
  name: string;
  type?: string;
  value?: string;
  component: T;
  renderCaption?: RC;
  className?: string;
  renderCaptionProps?: GetComponentProps<RC>;
} & Omit<GetComponentProps<T>, 'onChange'>;

const parse = (value: any) => value;
const parseNumber = (value: string) => (isNaN(parseFloat(value)) ? '' : parseFloat(value));

// eslint-disable-next-line react/function-component-definition
export function FormField<T = FC, RC = FC>({
  name,
  component,
  type,
  value,
  ...restProps
}: FormFieldProps<T, RC>) {
  const {
    input,
    meta: { error: validationError, submitError, ...meta },
  } = useField(name, { type, value, parse: type === 'number' ? parseNumber : parse });

  const error = useMemo(() => {
    // always show submitError even if field didn't "touched"
    if (submitError) return submitError as string;

    return meta.touched ? (validationError as string) : undefined;
  }, [meta, submitError, validationError]);

  const Component = component as any;

  return <Component<RC> {...restProps} {...input} error={error} />;
}
