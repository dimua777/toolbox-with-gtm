import { FC } from 'react';
import InputMask from 'react-input-mask';

import { TextField, TextFieldProps } from '../text-field';

type MaskedFieldProps = Omit<TextFieldProps, 'onChange'> & {
  mask: string | Array<RegExp | string>;
  alwaysShowMask?: boolean;
  maskPlaceholder?: string;
  onChange: (event: any) => void;
};

export const MaskedField: FC<MaskedFieldProps> = ({ size, ...rest }) => {
  return (
    <InputMask {...rest}>
      <TextField size={size} />
    </InputMask>
  );
};
