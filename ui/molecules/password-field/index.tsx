import { FC } from 'react';
import { useToggle } from 'react-use';

import { Icon } from '@sh/ui/atoms';

import { TextField, TextFieldProps } from '../text-field';

export const PasswordField: FC<Omit<TextFieldProps, 'type'>> = props => {
  const [showPassword, toggleShowPassword] = useToggle(false);

  return (
    <TextField type={showPassword ? 'text' : 'password'} {...props}>
      <button
        className="focus:text-accent900 absolute z-10 right-1 top-1/2 outline-none focus:opacity-100 hover:opacity-70 transform -translate-y-1/2 transition"
        type="button"
        onClick={toggleShowPassword}
      >
        <span className="sr-only">toggle password visibility</span>
        <Icon
          name={showPassword ? 'visibility-on' : 'visibility-off'}
          size={props.size === 'lg' ? 20 : 16}
        />
      </button>
    </TextField>
  );
};
