import { ChangeEvent, FC, ReactNode } from 'react';
import styled from 'astroturf/react';

import { FieldError, Icon } from '../../atoms';

type CheckboxType = 'checkbox' | 'radio';

export type CheckboxFieldProps = {
  checked?: boolean;
  className?: string;
  error?: string;
  label?: ReactNode;
  name: string;
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  type?: CheckboxType;
  value?: string;
};

const RadioIcon = styled.div`
  @apply rounded-full bg-bg100;
  width: 12px;
  height: 12px;
`;

const CheckboxControl = styled.div<{ type: CheckboxType }>`
  @apply relative flex justify-center items-center transition-colors duration-300;
  @apply rounded border-2 border-outline200;
  @apply w-3 h-3;

  &.type-radio {
    @apply rounded-full;
  }

  svg,
  ${RadioIcon} {
    @apply transition-opacity duration-300 opacity-0;
  }
`;

const CheckboxWrapper = styled.label`
  @apply flex items-center gap-1 relative;
  @apply text-txt700;

  &:hover {
    @apply text-accent900;

    ${CheckboxControl} {
      @apply border-accent900;
    }

    input:checked ${CheckboxControl} {
      @apply bg-accent700;
    }

    input:checked ~ span {
      @apply text-accent700;
    }
  }

  input {
    &:focus {
      + ${CheckboxControl} {
        @apply ring-2 ring-outline400;
      }
    }

    &:checked {
      + ${CheckboxControl} {
        @apply bg-accent800 border-accent800;

        svg,
        ${RadioIcon} {
          @apply opacity-100;
        }
      }
    }

    &:disabled {
      + ${CheckboxControl}, ~ span {
        @apply opacity-50;
      }
    }
  }
`;

export const CheckboxField: FC<CheckboxFieldProps> = ({
  label,
  name,
  type = 'checkbox',
  className,
  error,
  ...rest
}) => {
  const htmlType = type === 'radio' ? 'radio' : 'checkbox';

  return (
    <CheckboxWrapper className={className}>
      <input className="sr-only" name={name} type={htmlType} {...rest} />
      <CheckboxControl type={type}>
        {type === 'checkbox' && <Icon className="text-bg100" name="check" size={16} />}
        {type === 'radio' && <RadioIcon />}
      </CheckboxControl>
      <span>{label}</span>
      {error && <FieldError>{error}</FieldError>}
    </CheckboxWrapper>
  );
};
