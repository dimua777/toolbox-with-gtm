/* eslint-disable @typescript-eslint/no-var-requires */
const fs = require('fs');

const plopFileName = 'index.js';
const plopDir = '.plop';
const packagesPath = '.';

// Return array names of directories from root directory by path
const getDirectories = path =>
  fs.readdirSync(path).filter(file => fs.statSync(`${path}/${file}`).isDirectory());

// Return module if exist
const getModuleIfExist = packagePath => {
  let isExist = false;

  try {
    isExist = fs.readdirSync(`${packagesPath}/${packagePath}/${plopDir}`).includes(plopFileName);
  } catch (e) {}

  return isExist ? require(`${packagesPath}/${packagePath}/${plopDir}/${plopFileName}`) : null;
};

// Create modify plop file (generator name with packageName)
const createCustomPlop = (plop, packageName) => ({
  ...plop,
  setGenerator: (name, config) => plop.setGenerator(`${packageName} | ${name}`, config),
});

// Return max length from array
const getMaxLength = list =>
  list.reduce((acc, item) => {
    if (item.length > acc) return item.length;
    return acc;
  }, 0);

// Append backspace if name smaller length
const nameByNameLength = (name, length) => {
  if (name.length === length) return name;
  return `${name}${new Array(length - name.length + 1).join(' ')}`;
};

module.exports = plop => {
  // get packages directories
  const packageDirectories = getDirectories(packagesPath);

  // skip packages without plop module
  const dataConfigs = packageDirectories
    .map(packageName => ({ packageName, module: getModuleIfExist(packageName) }))
    .filter(config => !!config.module);

  const maxNameLength = getMaxLength(dataConfigs.map(({ packageName }) => packageName));

  // Call plop modules
  dataConfigs.forEach(({ packageName, module }) =>
    module(createCustomPlop(plop, nameByNameLength(packageName, maxNameLength))),
  );
};
