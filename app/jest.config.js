/* eslint-disable @typescript-eslint/no-var-requires */

const packageName = require('./package.json').name.split('@sh/').pop();

module.exports = {
  displayName: packageName,
  name: packageName,
  // .spec only for Cypress
  testMatch: ['**/*.(test).(js|ts|tsx)'],
};
