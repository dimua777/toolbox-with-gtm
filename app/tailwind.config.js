/* eslint-disable @typescript-eslint/no-var-requires */
module.exports = {
  purge: [
    '../ui/**/*.{js,ts,jsx,tsx}',
    './features/**/*.{js,ts,jsx,tsx}',
    './pages/**/*.{js,ts,jsx,tsx}',
  ],
  presets: [require('../ui/tailwind.config')],
};
