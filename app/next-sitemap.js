module.exports = {
  siteUrl: process.env.NEXT_PUBLIC_APP_URL,
  generateRobotsTxt: true,
  // TODO: exclude all private pages (you also can use wildcard matching)
  exclude: [],
  robotsTxtOptions: {
    policies: [
      {
        userAgent: '*',
        allow: '/',
      },
    ],
    additionalSitemaps: [
      // TODO: Replace with actual backend sitemap if exist.
      // `${process.env.NEXT_PUBLIC_APP_URL}/backend-sitemap.xml`,
    ],
  },
};
