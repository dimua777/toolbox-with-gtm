import Link from 'next/link';
import { NextSeo } from 'next-seo';

import { RouteBlockerLayout } from 'features/test/route-blocker-layout';

const Page2 = () => {
  return (
    <>
      <NextSeo title="Test useRouteBlocker!" />
      <h1 className="v-bs-h300" data-cy="page-title">
        Test useRouteBlocker! --- Page 2
      </h1>

      <Link href="/test-pages/test-use-route-blocker" passHref>
        <a
          className="bg-outline300 hover:bg-outline400 block px-2 py-1 rounded-md"
          data-cy="page-link"
        >
          Redirect back
        </a>
      </Link>
    </>
  );
};

Page2.getLayout = RouteBlockerLayout;

export default Page2;
