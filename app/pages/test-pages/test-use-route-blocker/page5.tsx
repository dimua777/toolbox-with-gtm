import { FC } from 'react';
import { Field, Form, FormRenderProps } from 'react-final-form';
import Link from 'next/link';
import { NextSeo } from 'next-seo';

import { useRouteBlocker } from '@sh/core';

import { RouteBlockerLayout } from 'features/test/route-blocker-layout';

const Form1Render: FC<FormRenderProps> = ({ handleSubmit, dirty, submitting }) => {
  useRouteBlocker(dirty && !submitting, {
    allowedUrls: ['/test-pages/test-use-route-blocker/page3'],
  });
  return (
    <form className="flex flex-col p-2 border rounded-md" onSubmit={handleSubmit}>
      <div className="flex flex-col py-1">
        {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
        <label className="v-bs-input-label200 pb-1">
          First Name
          <Field
            className="v-bs-input-val200 p-1 w-full border-2 rounded-md"
            component="input"
            data-cy="f-name"
            name="firstName"
            placeholder="First Name"
          />
        </label>
      </div>
      <div className="py-2">
        <button className="bg-accent200 p-2 rounded-md" type="submit">
          Submit
        </button>
      </div>
    </form>
  );
};

const Form2Render: FC<FormRenderProps> = ({ handleSubmit, dirty, submitting }) => {
  useRouteBlocker(dirty && !submitting, {
    allowedUrls: ['/test-pages/test-use-route-blocker/page4'],
  });
  return (
    <form className="flex flex-col p-2 border rounded-md" onSubmit={handleSubmit}>
      <div className="flex flex-col py-1">
        {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
        <label className="v-bs-input-label200 pb-1">
          Second Name
          <Field
            className="v-bs-input-val200 p-1 w-full border-2 rounded-md"
            component="input"
            data-cy="s-name"
            name="secondName"
            placeholder="Second Name"
          />
        </label>
      </div>
      <div className="py-2">
        <button className="bg-accent200 p-2 rounded-md" type="submit">
          Submit
        </button>
      </div>
    </form>
  );
};

const Page5 = () => {
  return (
    <>
      <NextSeo title="Test multi useRouteBlocker!" />
      <nav className="px-1 py-3">
        <ul className="flex space-x-2">
          <li>
            <Link href="/test-pages/test-use-route-blocker/page2" passHref>
              <a
                className="bg-outline300 hover:bg-outline400 block px-2 py-1 rounded-md"
                data-cy="page-link"
              >
                Page 2
              </a>
            </Link>
          </li>
          <li>
            <Link href="/test-pages/test-use-route-blocker/page3" passHref>
              <a
                className="bg-outline300 hover:bg-outline400 block px-2 py-1 rounded-md"
                data-cy="page-link"
              >
                Page 3
              </a>
            </Link>
          </li>
          <li>
            <Link href="/test-pages/test-use-route-blocker/page4" passHref>
              <a
                className="bg-outline300 hover:bg-outline400 block px-2 py-1 rounded-md"
                data-cy="page-link"
              >
                Page 4
              </a>
            </Link>
          </li>
        </ul>
      </nav>
      <h1 className="v-bs-h300" data-cy="page-title">
        Test multi useRouteBlocker!
      </h1>

      <div className="my-3">
        <Form render={Form1Render} onSubmit={() => {}} />
      </div>

      <div className="my-3">
        <Form render={Form2Render} onSubmit={() => {}} />
      </div>
    </>
  );
};

Page5.getLayout = RouteBlockerLayout;

export default Page5;
