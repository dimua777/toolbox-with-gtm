import { NextSeo } from 'next-seo';

import { RouteBlockerLayout } from 'features/test/route-blocker-layout';

const Page4 = () => {
  return (
    <>
      <NextSeo title="Test useRouteBlocker!" />
      <h1 className="v-bs-h300" data-cy="page-title">
        Test useRouteBlocker! --- Page 4
      </h1>
    </>
  );
};

Page4.getLayout = RouteBlockerLayout;

export default Page4;
