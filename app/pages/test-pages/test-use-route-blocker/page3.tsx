import { NextSeo } from 'next-seo';

import { RouteBlockerLayout } from 'features/test/route-blocker-layout';

const Page3 = () => {
  return (
    <>
      <NextSeo title="Test useRouteBlocker!" />
      <h1 className="v-bs-h300" data-cy="page-title">
        Test useRouteBlocker! --- Page 3
      </h1>
    </>
  );
};

Page3.getLayout = RouteBlockerLayout;

export default Page3;
