import Link from 'next/link';
import { NextSeo } from 'next-seo';

import { redirect, usePrevPage } from '@sh/core';

import { TestForm } from 'features/test/form';

// FIXME: remove demo page
const Index: NextPage = () => {
  const { prevPage } = usePrevPage();

  return (
    <div className="container">
      <NextSeo title="Hello Test!" />
      <h1>Hello Test!</h1>
      <br />
      <Link href={prevPage}>Back Button</Link>
      <br />
      <br />
      <ul>
        <li>
          <Link href="/test-pages?what=where">test link with query</Link>
        </li>
        <li>
          <Link href="/test-pages?why=because">test link with query 2</Link>
        </li>
        <li>
          <Link href="/test-pages?dfdf=fgfdg">test link with query 3</Link>
        </li>
      </ul>
      <br />
      <button type="button" onClick={() => redirect('/')}>
        redirect /
      </button>

      <div className="p-2">
        <h2 className="my-2">Test Form</h2>
        <TestForm />
      </div>
    </div>
  );
};

Index.getLayout = page => (
  <div>
    <h1 className="text-accent900 v-bs-h900">Layout test</h1>
    <a href="https://nextjs.org/docs/basic-features/layouts#per-page-layouts">
      https://nextjs.org/docs/basic-features/layouts#per-page-layouts
    </a>
    <br />
    <br />
    <br />
    {page}
  </div>
);

export default Index;
