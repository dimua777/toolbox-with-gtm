import { useEffect } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { css } from 'astroturf';
import testImage from 'public/android-chrome-512x512.png';

import { pushToDataLayer } from '@sh/core';
import { TextButton } from '@sh/ui';

// FIXME: remove demo page
const Index = () => {
  useEffect(() => {
    pushToDataLayer({ event: 'login', method: 'push' });
  }, []);
  return (
    <>
      <div>test static image</div>
      <Image alt="logo" placeholder="blur" src={testImage} />

      <div className="container">
        container
        <div className="gs mt-4">
          <div className="bg-accent200">grid system 1</div>
          <div className="bg-accent200 col-span-6">grid system 2</div>
          <div className="bg-accent200">grid system 3</div>
          <div className="bg-accent200 col-span-full">
            grid system 4
            <br />
            <i className="italic font-bold">
              * pay attention that count of grid columns are different on various screen resolutions
            </i>
            <br />
            <i className="italic font-bold">
              ** you just should enable "Show layout grids" in Figma:{' '}
              <TextButton as="a" href="https://monosnap.com/file/qwxbNG0TOdXf51jhZ1RmOTb7xUba0d">
                https://monosnap.com/file/qwxbNG0TOdXf51jhZ1RmOTb7xUba0d
              </TextButton>
            </i>
          </div>
        </div>
      </div>
      <div className="p-4">
        <div className="line-clamp-3 mb-3">
          line-clamp plugin line-clamp plugin line-clamp plugin line-clamp plugin line-clamp plugin
          line-clamp plugin line-clamp plugin line-clamp plugin line-clamp plugin line-clamp plugin
          line-clamp plugin line-clamp plugin line-clamp plugin line-clamp plugin line-clamp plugin
          line-clamp plugin line-clamp plugin line-clamp plugin line-clamp plugin line-clamp plugin
          line-clamp plugin line-clamp plugin line-clamp plugin line-clamp plugin line-clamp plugin
          line-clamp plugin line-clamp plugin line-clamp plugin line-clamp plugin line-clamp plugin
          line-clamp plugin line-clamp plugin line-clamp plugin line-clamp plugin line-clamp plugin
          line-clamp plugin line-clamp plugin line-clamp plugin line-clamp plugin line-clamp plugin
          line-clamp plugin line-clamp plugin line-clamp plugin line-clamp plugin line-clamp plugin
          line-clamp plugin line-clamp plugin line-clamp plugin
        </div>

        <div className="aspect-w-16 aspect-h-9">
          <div className="bg-accent900 text-color200 flex items-center justify-center">
            aspect ratio plugin
          </div>
        </div>

        <div className="text-primaryBlue900">Box</div>
        <div
          className="bg-primaryBlue920"
          css={css`
            @apply mt-2;
          `}
        >
          astroturf
        </div>
        <input className="border-accent900 text-xs border outline-none focus:ring" type="text" />
        <Link href="/test-pages">
          <a className="block mt-3">go /test-pages</a>
        </Link>
      </div>
    </>
  );
};

export default Index;
