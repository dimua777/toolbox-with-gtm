type NextPage<P = Record<string, unknown>, IP = P> = import('next').NextComponentType<
  import('next').NextPageContext,
  IP,
  P
> & {
  getLayout?(page: import('react').ReactNode): import('react').ReactNode;
};
