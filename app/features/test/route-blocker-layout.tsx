import { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/router';

import { RouteBlockerProvider } from '@sh/core';

export const RouteBlockerLayout = (page: ReactNode) => {
  const [blocked, setBlocked] = useState(false);
  const callbackRef = useRef<(result: boolean) => void>();
  const handlerOfBlocker = useCallback(cb => {
    callbackRef.current = cb;
    setBlocked(true);
  }, []);

  // After change route to reset state of blocked information
  const router = useRouter();
  useEffect(() => {
    const handleRouteChange = () => {
      setBlocked(false);
    };

    router.events.on('routeChangeStart', handleRouteChange);

    return () => {
      router.events.off('routeChangeStart', handleRouteChange);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <RouteBlockerProvider handlerOfBlocker={handlerOfBlocker}>
      <div className="p-2">
        {page}

        {blocked && (
          <div className="border-error100 p-3 border-2 rounded-md" data-cy="blocker-message">
            I am the result of blocked router{' '}
            <button
              className="bg-accent200 p-2 rounded-md"
              data-cy="force-redirect"
              type="button"
              onClick={e => {
                e.preventDefault();
                if (callbackRef.current) {
                  callbackRef.current(true);
                }
              }}
            >
              Force redirect
            </button>
          </div>
        )}
      </div>
    </RouteBlockerProvider>
  );
};
