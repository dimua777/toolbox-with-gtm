import { FC, useCallback } from 'react';
import { Form, FormRenderProps } from 'react-final-form';

import {
  defaultSubscription,
  handleMutationErrors,
  InferType,
  object,
  string,
  useRouteBlocker,
  validateFormValues,
} from '@sh/core';
import { Button, CheckboxField, FormField, TextField } from '@sh/ui';

const schema = object()
  .shape({
    email: string().email().required(),
  })
  .required();

type SchemaType = InferType<typeof schema>;

const validate = validateFormValues<SchemaType>(schema);

const sleep = (ms: number) => new Promise(res => setTimeout(res, ms));

// FIXME: Add real submit handler
const requestPasswordReq = async ({ variables: { input } }: any) => {
  await sleep(1000);
  console.log(input);
};

const TestFormContent = ({
  handleSubmit,
  submitting,
  submitError,
  dirty,
}: FormRenderProps<SchemaType>) => {
  useRouteBlocker(dirty);

  return (
    <form className="grid gap-3" onSubmit={handleSubmit}>
      <h1>Test from with useRouteBlocker</h1>
      <FormField
        component={TextField}
        label="Email"
        name="email"
        placeholder="Enter small size email"
        size="sm"
        type="email"
      />

      <FormField component={TextField} name="name" placeholder="Enter name" type="text" />

      <FormField component={CheckboxField} label="Subscribe to newsletter?" name="newsletter" />

      <div className="flex gap-2">
        <FormField component={CheckboxField} label="Male" name="gender" type="radio" value="male" />
        <FormField
          component={CheckboxField}
          label="Female"
          name="gender"
          type="radio"
          value="female"
        />
      </div>

      {submitError && (
        <div className="text-error100 v-bs-input-label200 text-center">{submitError}</div>
      )}

      <Button loading={submitting} type="submit">
        Send instructions
      </Button>
    </form>
  );
};

export const TestForm: FC = () => {
  const onSubmit = useCallback((input: SchemaType) => {
    return requestPasswordReq({ variables: { input } }).catch(handleMutationErrors);
  }, []);

  return (
    <Form
      render={TestFormContent}
      subscription={defaultSubscription}
      validate={validate}
      onSubmit={onSubmit}
    />
  );
};
