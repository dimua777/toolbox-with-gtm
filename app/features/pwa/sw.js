import { precacheAndRoute } from 'workbox-precaching';
import { registerRoute } from 'workbox-routing';
import { NetworkFirst } from 'workbox-strategies';

// inject precache manifest
precacheAndRoute(self.__WB_MANIFEST);

addEventListener('message', event => {
  if (event.data && event.data.type === 'SKIP_WAITING') {
    skipWaiting();
  }
});

// TODO: more accurate routing
registerRoute(/.*/, new NetworkFirst(), 'GET');
