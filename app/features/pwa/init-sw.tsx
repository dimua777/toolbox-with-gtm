import { Workbox } from 'workbox-window';

const onUpdateAvailable = (wb: Workbox) => {
  const skipWaiting = () => {
    wb.messageSW({ type: 'SKIP_WAITING' });
  };

  // SKIP_WAITING if just reload page too
  window.onbeforeunload = skipWaiting;

  const updateApp = () => {
    wb.addEventListener('controlling', () => {
      window.location.reload();
    });

    skipWaiting();
  };

  // FIXME: show nice confirm modal
  if (confirm('We have a new version for you! Update?')) {
    updateApp();
  }
};

export const initSW = () => {
  if (process.browser && process.env.NODE_ENV === 'production' && 'serviceWorker' in navigator) {
    window.addEventListener('load', () => {
      const wb = new Workbox('/service-worker.js');

      wb.register();

      // listen for updates
      wb.addEventListener('waiting', () => onUpdateAvailable(wb));

      // check updates every 30sec
      setInterval(() => {
        navigator.serviceWorker.getRegistration().then(reg => reg?.update());
      }, 30 * 1000);
    });
  }
};
