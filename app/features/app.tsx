import { useCallback } from 'react';
import { NextPage } from 'next';
import Head from 'next/head';
import { DefaultSeo } from 'next-seo';

import {
  addPageProgressListener,
  GoogleTagManager,
  PrevPageProvider,
  RouteBlockerProvider,
} from '@sh/core';
import { UIProvider } from '@sh/ui';

import { favicon } from './layout';
import { initSW } from './pwa';

initSW();
addPageProgressListener();

const additionalMetaTags = [
  {
    name: 'viewport',
    content: 'width=device-width,initial-scale=1,maximum-scale=2,shrink-to-fit=no',
    // FIXME: choose 'content' depending on project needs
    // if input's font size lower than 16px we can use this viewport, but it's not accessibility friendly
    // content:
    //   'minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no, viewport-fit=cover, maximum-scale=1, user-scalable=no',
  },
];

const getDefaultLayout = (page: any) => page;

/**
 * FIXME: route blocker issues https://take.ms/gtkLn:
 * - Cannot update a component (`Constate`) while rendering a different component (`ReactFinalForm`).
 * - When useRouteBlocker unmounted we need update `enabled`
 * https://take.ms/gtkLn
 * - think about testing core with cypress on real projects.. (we can't store "test pages" in main app)
 */

export const App: NextPage<any> = ({ Component, pageProps }) => {
  const getLayout = Component.getLayout || getDefaultLayout;

  const handlerOfBlocker = useCallback(cb => {
    cb(confirm('you have unsaved changes... leave?'));
  }, []);

  return (
    <>
      <Head>{favicon}</Head>
      <DefaultSeo
        additionalMetaTags={additionalMetaTags}
        description="FIXME: provide description"
        title="Page title"
        titleTemplate="%s | FIXME: {projectName}"
      />
      <GoogleTagManager gtmID={process.env.NEXT_PUBLIC_GOOGLE_TAG_MANAGER_ID as string} />

      <UIProvider>
        <PrevPageProvider>
          <RouteBlockerProvider handlerOfBlocker={handlerOfBlocker}>
            {getLayout(<Component {...pageProps} />)}
          </RouteBlockerProvider>
        </PrevPageProvider>
      </UIProvider>
    </>
  );
};
