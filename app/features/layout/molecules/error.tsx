import { FC } from 'react';
import Link from 'next/link';

import { Button } from '@sh/ui';

type ErrorProps = {
  title: string;
  message: string;
  statusCode: number;
};

export const Error: FC<ErrorProps> = ({ statusCode, message, children }) => (
  <div className="container flex flex-col items-center justify-center py-3 min-h-screen text-center">
    <h1 className="v-bs-hero900 text-txt900">
      {statusCode
        .toString()
        .split('')
        .map((char, index) =>
          index === 1 ? (
            <span key={index} className="text-accent900">
              {char}
            </span>
          ) : (
            char
          ),
        )}
    </h1>
    <p className="v-bs-p700 text-txt700" css="max-width: 468px">
      {message}
    </p>
    <Link href="/" passHref>
      <Button as="a" className="mt-4">
        Back to home
      </Button>
    </Link>
    {children}
  </div>
);
