export const favicon = (
  <>
    <link href="/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180" />
    <link href="/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png" />
    <link href="/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png" />
    <link href="/site.webmanifest" rel="manifest" />
    <link color="#1f1f2f" href="/safari-pinned-tab.svg" rel="mask-icon" />
    <meta content="FIXME: {projectName}" name="apple-mobile-web-app-title" />
    <meta content="FIXME: {projectName}" name="application-name" />
    <meta content="#1f1f2f" name="msapplication-TileColor" />
    <meta content="#1F1F2F" name="theme-color" />
  </>
);
