import { FC } from 'react';
import { NextSeo } from 'next-seo';

import { Error } from '../molecules/error';

const statusCodes: Record<number, { title: string; message: string }> = {
  403: {
    title: 'Forbidden',
    message: "You don't have permission to see this content",
  },
  404: {
    title: 'Not found',
    message: 'The link you followed may be broken, or the page may have been removed.',
  },
  500: { title: 'Error', message: 'Internal Server Error' },
};

export const ErrorPage: FC<{ statusCode: number }> = props => {
  const msgs = statusCodes[props.statusCode] || statusCodes[500];

  return (
    <>
      <NextSeo title={msgs.title} />
      <Error {...msgs} {...props} />
    </>
  );
};
