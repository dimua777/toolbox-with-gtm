/* eslint-disable @typescript-eslint/no-var-requires */
const { compose } = require('ramda');
const CircularDependencyPlugin = require('circular-dependency-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const zlib = require('zlib');

const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});
const withOffline = require('next-offline');
const withTM = require('next-transpile-modules')(['@sh/core', '@sh/ui']);

const enhance = compose(withTM, withOffline, withBundleAnalyzer);

/**
 * @type {import('next/dist/next-server/server/config').NextConfig}
 **/
const nextConfig = {
  compress: false, // nginx should care about gzip,

  future: {
    strictPostcssConfiguration: true,
  },

  i18n: {
    locales: ['en'],
    defaultLocale: 'en',
  },

  webpack(config, { dev, isServer }) {
    config.module.rules.push({
      test: /\.tsx$/,
      use: [
        {
          loader: 'astroturf/loader',
          options: { extension: '.module.css', writeFiles: true },
        },
      ],
    });

    config.plugins.push(
      new CircularDependencyPlugin({
        cwd: process.cwd(),
        exclude: /node_modules/,
        failOnError: true,
      }),
    );

    config.module.rules.push({
      test: /\.svg$/,
      type: 'asset/resource',
      generator: {
        filename: 'static/media/[name]-[hash].[ext]',
      },
    });

    if (!dev && !isServer) {
      config.plugins.push(
        new CompressionPlugin({
          filename: '[path][base].br',
          algorithm: 'brotliCompress',
          test: /\.(js|css|html|svg)$/,
          compressionOptions: {
            params: {
              [zlib.constants.BROTLI_PARAM_QUALITY]: 11,
            },
          },
          threshold: 500,
          minRatio: 0.8,
          deleteOriginalAssets: false,
        }),
      );

      // dedup lodash
      config.resolve.alias = {
        ...config.resolve.alias,
        lodash: 'lodash-es',
      };
    }

    return config;
  },

  async redirects() {
    return [
      // To follow best practice https://web.dev/change-password-url/
      {
        source: '/.well-known/change-password',
        destination: '/me/security', // FIXME: replace with real change password page
        permanent: false,
      },
    ];
  },

  async rewrites() {
    return {
      beforeFiles: [
        {
          source: '/service-worker.js',
          // local rewrites + i18n currently causing errors https://github.com/vercel/next.js/discussions/19707
          // TODO: fix when issue will be resolved
          destination: `${process.env.NEXT_PUBLIC_APP_URL}/_next/static/service-worker.js`,
        },
      ],

      fallback: process.env.NEXT_PROXY_FALLBACK
        ? [
            {
              source: '/:path*',
              destination: `${process.env.NEXT_PROXY_FALLBACK}/:path*`,
            },
          ]
        : [],
    };
  },
};

module.exports = enhance({
  ...nextConfig,

  // next-offline options
  dontAutoRegisterSw: true,
  generateSw: false,
  workboxOpts: {
    swSrc: './features/pwa/sw.js',
    swDest: 'static/service-worker.js',
    exclude: [/.map$/g, /.wasm$/g, /.json$/g],
  },
});
