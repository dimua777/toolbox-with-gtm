/// <reference types="cypress" />

context('useRouteBlocker', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3001/test-pages/test-use-route-blocker');
  });

  // https://on.cypress.io/interacting-with-elements

  it('should leave current page', () => {
    // https://on.cypress.io/type
    cy.getBySel('f-name').should('have.value', '');

    cy.getBySel('page-link').eq(0).click();

    cy.wait(1000);

    cy.url().should('include', '/test-pages/test-use-route-blocker/page2');
    cy.getBySel('page-title').should('contain.text', 'Test useRouteBlocker! --- Page 2');
  });

  it('should not leave current page', () => {
    // https://on.cypress.io/focus
    cy.getBySel('f-name').type('any text').should('have.value', 'any text');

    cy.getBySel('page-link').eq(0).click();

    cy.getBySel('blocker-message').should('contain.text', 'I am the result of blocked router');

    cy.url().should('include', '/test-pages/test-use-route-blocker');
    cy.url().should('not.include', '/page2');
  });

  it('should leave current page for allowed urls', () => {
    // https://on.cypress.io/blur
    cy.getBySel('f-name').type('any text').should('have.value', 'any text');

    cy.getBySel('page-link').eq(1).click();

    cy.wait(1000);

    cy.url().should('include', '/test-pages/test-use-route-blocker/page3');
    cy.getBySel('page-title').should('contain.text', 'Test useRouteBlocker! --- Page 3');
  });

  it('should leave current page for allowed urls of useRouteBlockerAllowList hook', () => {
    // https://on.cypress.io/blur
    cy.getBySel('f-name').type('any text').should('have.value', 'any text');

    cy.getBySel('page-link').eq(2).click();

    cy.wait(1000);

    cy.url().should('include', '/test-pages/test-use-route-blocker/page4');
    cy.getBySel('page-title').should('contain.text', 'Test useRouteBlocker! --- Page 4');
  });

  it('should leave current page forcefully with blocked router', () => {
    // https://on.cypress.io/blur
    cy.getBySel('f-name').type('any text').should('have.value', 'any text');

    cy.getBySel('page-link').eq(0).click();

    cy.wait(1000);

    cy.getBySel('blocker-message').should('contain.text', 'I am the result of blocked router');

    cy.url().should('include', '/test-pages/test-use-route-blocker');
    cy.url().should('not.include', '/page2');

    cy.getBySel('force-redirect').click();

    cy.wait(1000);

    cy.url().should('include', '/test-pages/test-use-route-blocker/page2');
    cy.getBySel('page-title').should('contain.text', 'Test useRouteBlocker! --- Page 2');
  });

  it('should leave current page forcefully with blocked router and come back', () => {
    // https://on.cypress.io/blur
    cy.getBySel('f-name').type('any text').should('have.value', 'any text');

    cy.getBySel('page-link').eq(0).click();

    cy.wait(1000);

    cy.getBySel('blocker-message').should('contain.text', 'I am the result of blocked router');

    cy.url().should('include', '/test-pages/test-use-route-blocker');
    cy.url().should('not.include', '/page2');

    cy.getBySel('force-redirect').click();

    cy.wait(1000);

    cy.url().should('include', '/test-pages/test-use-route-blocker/page2');
    cy.getBySel('page-title').should('contain.text', 'Test useRouteBlocker! --- Page 2');

    cy.getBySel('page-link').should('contain.text', 'Redirect back').click();

    cy.wait(1000);

    cy.url().should('include', '/test-pages/test-use-route-blocker');
    cy.url().should('not.include', '/page2');
  });

  describe('multi running', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3001/test-pages/test-use-route-blocker/page5');
    });

    it('should block two pages of three when a user uses one blocker', () => {
      cy.getBySel('f-name').type('any text').should('have.value', 'any text');

      cy.getBySel('page-link').as('pageLinks');

      cy.get('@pageLinks').eq(0).click();

      cy.wait(1000);

      cy.getBySel('blocker-message').should('contain.text', 'I am the result of blocked router');

      cy.url().should('include', '/test-pages/test-use-route-blocker/page5');
      cy.url().should('not.include', '/page2');

      cy.get('@pageLinks').eq(2).click();

      cy.wait(1000);

      cy.getBySel('blocker-message').should('contain.text', 'I am the result of blocked router');

      cy.url().should('include', '/test-pages/test-use-route-blocker/page5');
      cy.url().should('not.include', '/page4');

      cy.get('@pageLinks').eq(1).click();

      cy.wait(1000);

      cy.url().should('include', '/test-pages/test-use-route-blocker/page3');
    });

    it('should block one page of three when a user uses two blocker at the same time', () => {
      cy.getBySel('f-name').type('any text').should('have.value', 'any text');
      cy.getBySel('s-name').type('any text').should('have.value', 'any text');

      cy.getBySel('page-link').as('pageLinks');

      cy.get('@pageLinks').eq(0).click();

      cy.wait(1000);

      cy.getBySel('blocker-message').should('contain.text', 'I am the result of blocked router');

      cy.url().should('include', '/test-pages/test-use-route-blocker/page5');
      cy.url().should('not.include', '/page2');

      cy.get('@pageLinks').eq(1).click();

      cy.wait(1000);

      cy.url().should('include', '/test-pages/test-use-route-blocker/page3');

      cy.go('back');

      cy.url().should('include', '/test-pages/test-use-route-blocker/page5');

      cy.getBySel('f-name').type('any text').should('have.value', 'any text');
      cy.getBySel('s-name').type('any text').should('have.value', 'any text');

      cy.getBySel('page-link').eq(2).click();

      cy.wait(1000);

      cy.url().should('include', '/test-pages/test-use-route-blocker/page4');
    });

    it('should have different blocked pages if one blocker changes its state', () => {
      cy.getBySel('f-name').type('any text').should('have.value', 'any text');
      cy.getBySel('s-name').clear('').should('have.value', '');

      cy.getBySel('page-link').eq(0).click();

      cy.wait(1000);

      cy.getBySel('blocker-message').should('contain.text', 'I am the result of blocked router');

      cy.url().should('include', '/test-pages/test-use-route-blocker/page5');
      cy.url().should('not.include', '/page2');

      cy.getBySel('page-link').eq(2).click();

      cy.wait(1000);

      cy.getBySel('blocker-message').should('contain.text', 'I am the result of blocked router');

      cy.url().should('include', '/test-pages/test-use-route-blocker/page5');

      cy.getBySel('s-name').type('any text').should('have.value', 'any text');

      cy.getBySel('page-link').eq(0).click();

      cy.wait(1000);

      cy.getBySel('blocker-message').should('contain.text', 'I am the result of blocked router');

      cy.url().should('include', '/test-pages/test-use-route-blocker/page5');

      cy.getBySel('page-link').eq(2).click();

      cy.url().should('include', '/test-pages/test-use-route-blocker/page4');
    });

    it('should have different blocked pages if both blockers change their states', () => {
      cy.getBySel('f-name').type('any text').should('have.value', 'any text');
      cy.getBySel('s-name').type('any text').should('have.value', 'any text');

      cy.getBySel('page-link').eq(0).click();

      cy.wait(1000);

      cy.getBySel('blocker-message').should('contain.text', 'I am the result of blocked router');

      cy.url().should('include', '/test-pages/test-use-route-blocker/page5');
      cy.url().should('not.include', '/page2');

      cy.getBySel('s-name').clear('').should('have.value', '');

      cy.getBySel('page-link').eq(0).click();

      cy.wait(1000);

      cy.getBySel('blocker-message').should('contain.text', 'I am the result of blocked router');

      cy.getBySel('f-name').clear('').should('have.value', '');

      cy.getBySel('page-link').eq(0).click();

      cy.url().should('include', '/test-pages/test-use-route-blocker/page2');
    });

    it('should not have blocked pages if there were been removed all blockers', () => {
      cy.getBySel('f-name').type('any text').should('have.value', 'any text');
      cy.getBySel('s-name').type('any text').should('have.value', 'any text');

      cy.getBySel('f-name').clear().should('have.value', '');
      cy.getBySel('s-name').clear().should('have.value', '');

      cy.getBySel('page-link').eq(0).click();

      cy.wait(1000);

      cy.url().should('include', '/test-pages/test-use-route-blocker/page2');

      cy.go('back');

      cy.getBySel('f-name').type('any text').should('have.value', 'any text');
      cy.getBySel('s-name').type('any text').should('have.value', 'any text');

      cy.getBySel('f-name').clear().should('have.value', '');
      cy.getBySel('s-name').clear().should('have.value', '');

      cy.getBySel('page-link').eq(1).click();

      cy.wait(1000);

      cy.url().should('include', '/test-pages/test-use-route-blocker/page3');

      cy.go('back');

      cy.getBySel('f-name').type('any text').should('have.value', 'any text');
      cy.getBySel('s-name').type('any text').should('have.value', 'any text');

      cy.getBySel('f-name').clear().should('have.value', '');
      cy.getBySel('s-name').clear().should('have.value', '');

      cy.getBySel('page-link').eq(2).click();

      cy.wait(1000);

      cy.url().should('include', '/test-pages/test-use-route-blocker/page4');

      cy.go('back');
    });
  });
});
