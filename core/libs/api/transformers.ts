import { assocPath } from 'ramda';

import { jsonApiDeserialize } from '../json-api-deserialize';

type RawAPIError = {
  title: string;
  code: 'INVALID' | 'INVALID_INPUT' | 'NOT_FOUND' | 'RECORD_INVALID' | 'UNAUTHORIZED';
  status: number;
  source?: {
    /**
     * for example for data:
     * { account: { email: 'example@email.com' } }
     * pointer will be:
     * '/data/attributes/account/email'
     */
    pointer: string;
  };
};

export const formatErrors = (data: RawAPIError[] | string = []) => {
  let error;
  let errors;

  if (typeof data === 'string') {
    error = data;
  } else {
    errors = data.reduce((acc, err) => {
      const path = err.source?.pointer.split(/\/|\./).filter(item => item !== '') ?? [''];

      if (path.length === 1 && path[0] === '') {
        error = err.title;
        return acc;
      }

      return assocPath(path.slice(3), err.title, acc);
    }, {});
  }

  return { errors, error };
};

export const transformRequest = (data: any) => JSON.stringify(data);

export const transformResponse = (transformer: any) => (data: string) => {
  if (!data) {
    return null;
  }

  try {
    const parsed = JSON.parse(data);

    const transformedResponse = transformer(parsed);

    const formatted = {
      payload: transformedResponse,
      meta: parsed.meta || {},
      /** assume error as fallback */
      ...formatErrors(parsed.errors || parsed.error),
    };

    return formatted;
  } catch (error) {
    return data;
  }
};

export const defaultResponseTransformer = transformResponse(jsonApiDeserialize);
