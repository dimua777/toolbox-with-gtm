import { AxiosError, AxiosResponse } from 'axios';

const errorNotifications = {
  byResponseStatus: {
    403: 'Sorry, but you don’t have permission to access this area. Try another account or connect to the support team.',
    404: 'Sorry, we couldn’t find that page. Please try to use searching or connect to the support team.',
    413: 'Oops, something went wrong, please try again or connect to the support team.',
    422: 'Violation of business rules/data integrity took place.',
    429: 'Too many requests. Try again later.',
    fallback: 'Oops, something went wrong, please try again or connect to the support team.',
  },
  offline: 'It looks like you offline',
};

const errorByStatus: { [key: string]: string } = errorNotifications.byResponseStatus;

const getFallbackMsg = (status: any, errors?: Record<string, string>) => {
  if (typeof navigator === 'object' && !navigator.onLine) {
    return errorNotifications.offline;
  }

  // show first error from errors (if exists) instead of default messages
  if (errors && Object.keys(errors).length > 0) {
    return Object.values(errors)[0];
  }

  return errorByStatus[status] || errorByStatus.fallback;
};

export const handleRes = (res: AxiosResponse) => {
  if (process.env.NODE_ENV === 'development') {
    const method = (res.config.method || 'get').toUpperCase();
    const styles: any = {
      GET: 'color: #61affe',
      POST: 'color: #49cc90',
      PUT: 'color: #fca130',
      DELETE: 'color: #f93e3e',
    };

    if (process.browser) {
      console.log(`%c${method}`, styles[method], res.config.url, res.data);
    } else {
      console.log(method, res.config.url, res.data);
    }
  }

  return res;
};

export const handleError = (e: AxiosError) => {
  const status = e?.response?.status;
  const errors = e?.response?.data?.errors ?? {};
  const error = e?.response?.data?.error ?? getFallbackMsg(status);

  const errorRes: APIError = {
    error,
    errors,
    payload: e.response?.data?.payload,
    statusCode: e.response?.status || 500,
  };

  if (process.env.NODE_ENV === 'development') {
    const method = (e.config.method || 'get').toUpperCase();
    const styles: any = {
      GET: 'color: #61affe',
      POST: 'color: #49cc90',
      PUT: 'color: #fca130',
      DELETE: 'color: #f93e3e',
    };

    if (process.browser) {
      console.log(`%cERROR:`, styles.DELETE, method, e.config.url, errorRes);
    } else {
      console.log(method, e.config.url, errorRes);
    }
  }

  return Promise.reject(errorRes);
};
