import { FC, useMemo } from 'react';
import { NextPageContext } from 'next';
import { ApolloClient, ApolloProvider, NormalizedCacheObject } from '@apollo/client';
import merge from 'deepmerge';
import isEqual from 'lodash/isEqual';

import { createGqlClient } from './create-gql-client';
import { GQL_STATE_PROP_NAME } from './helpers';

type Client = ApolloClient<NormalizedCacheObject>;

let apolloClient: Client;

// https://github.com/vercel/next.js/blob/canary/examples/with-apollo/lib/apolloClient.js
function initializeApollo(initialState: any, ctx?: NextPageContext) {
  const _apolloClient = apolloClient ?? createGqlClient(ctx);

  // If your page has Next.js data fetching methods that use Apollo Client, the initial state
  // gets hydrated here
  if (initialState) {
    // Get existing cache, loaded during client side data fetching
    const existingCache = _apolloClient.extract();

    // Merge the existing cache into data passed from getStaticProps/getServerSideProps
    const data = merge(initialState, existingCache, {
      // combine arrays using object equality (like in sets)
      arrayMerge: (destinationArray, sourceArray) => [
        ...sourceArray,
        ...destinationArray.filter(d => sourceArray.every(s => !isEqual(d, s))),
      ],
    });

    // Restore the cache with the merged data
    _apolloClient.cache.restore(data);
  }
  // For SSG and SSR always create a new Apollo Client
  if (typeof window === 'undefined') return _apolloClient;
  // Create the Apollo Client once in the client
  if (!apolloClient) apolloClient = _apolloClient;

  return _apolloClient;
}

const useApollo = (pageProps: any) => {
  const state = pageProps?.[GQL_STATE_PROP_NAME] ?? pageProps?.props?.[GQL_STATE_PROP_NAME];

  return useMemo(() => initializeApollo(state), [state]);
};

export const GraphqlProvider: FC<{ pageProps: any }> = ({ children, pageProps }) => {
  const client = useApollo(pageProps);

  return <ApolloProvider client={client}>{children}</ApolloProvider>;
};
