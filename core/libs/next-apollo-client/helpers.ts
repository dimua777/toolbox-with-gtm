import { ApolloClient, NormalizedCacheObject } from '@apollo/client';
import { ApolloError } from '@apollo/client/errors';
import { assocPath } from 'ramda';

export const GQL_STATE_PROP_NAME = '__GQL_STATE__';

export const addGraphqlState = <T>(client: ApolloClient<NormalizedCacheObject>, pageProps?: T) => {
  const rawProps: any = pageProps || {};

  // getInitialProps has 'pageProps'
  const propsKeyName = rawProps.pageProps ? 'pageProps' : 'props';

  return {
    ...rawProps,
    [propsKeyName]: { ...rawProps[propsKeyName], [GQL_STATE_PROP_NAME]: client.cache.extract() },
  } as T;
};

// https://gitlab.com/shakurocom/aruuuba/-/blob/master/backend/app/errors/error_codes.rb
export const formatMutationErrors = (error?: ApolloError) => {
  const errors = error?.graphQLErrors.reduce((acc, err) => {
    const validationError = err.extensions?.errors?.[0];
    const path = validationError?.path?.[0] ?? '';

    if (!path) return acc;

    return assocPath(path.split('.'), validationError?.messages?.[0], acc);
  }, {});

  return {
    errors,
    hasValidationErrors: Object.keys(errors ?? {}).length > 0,
    globalError: error?.message,
  };
};
