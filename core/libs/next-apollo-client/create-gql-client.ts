import { NextPageContext } from 'next';
import { ApolloClient, from, HttpLink, InMemoryCache } from '@apollo/client';
import { onError } from '@apollo/client/link/error';
import { relayStylePagination } from '@apollo/client/utilities';

const httpLink = (ctx?: NextPageContext) =>
  new HttpLink({
    credentials: 'include',
    headers: ctx?.req?.headers as any,
    uri: `${process.env.NEXT_PUBLIC_APP_URL}/api/graphql`,
  });

const errorLink = onError(({ graphQLErrors }) => {
  if (graphQLErrors) {
    // TODO: Agree with BE and add a code for outdated session error
    // graphQLErrors.map(({ message, extensions }) => {});
  }
});

export const createGqlClient = (ctx?: NextPageContext) =>
  new ApolloClient({
    cache: new InMemoryCache({
      typePolicies: {
        Query: {
          fields: {
            publicFeed: relayStylePagination(),
            userFeed: relayStylePagination(),
            mosques: relayStylePagination(),
          },
        },
      },
    }),
    connectToDevTools: process.env.NODE_ENV === 'development',
    link: from([errorLink, httpLink(ctx)]),
    ssrMode: !process.browser,
    defaultOptions: {
      watchQuery: {
        fetchPolicy: 'cache-and-network',
      },
    },
  });
