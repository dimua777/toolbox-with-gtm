/* eslint-disable max-lines */
import { jsonApiDeserialize } from './index';

describe('jsonApiDeserialize', () => {
  describe('primitive tests', () => {
    test('number', () => {
      const data = jsonApiDeserialize(1);
      expect(data).toEqual(1);
    });
    test('string', () => {
      const data = jsonApiDeserialize('test');
      expect(data).toEqual('test');
    });
    test('boolean', () => {
      const data = jsonApiDeserialize(true);
      expect(data).toBeTruthy();
    });
    test('null', () => {
      const data = jsonApiDeserialize(null);
      expect(data).toBeNull();
    });
    test('undefined', () => {
      const data = jsonApiDeserialize(undefined);
      expect(data).toBeUndefined();
    });
  });

  describe('difficult data', () => {
    test('Date', () => {
      const date = new Date();
      const data = jsonApiDeserialize(date);
      expect(data).toEqual(date);
    });
    test('simple array', () => {
      const array = [1, '2', true, null];
      const data = jsonApiDeserialize(array);
      expect(data).toEqual(array);
    });
    test('simple object', () => {
      const simpleObject = { key1: 1, key2: '2', key3: true, key4: null };
      const data = jsonApiDeserialize(simpleObject);
      expect(data).toEqual(simpleObject);
    });
  });

  describe('json api', () => {
    describe('object', () => {
      test('with only attributes', () => {
        const obj = {
          data: {
            id: 1,
            type: 'user',
            attributes: { key1: 1, key2: '2', key3: true, key4: null },
          },
        };
        const expectObj = { id: 1, type: 'user', key1: 1, key2: '2', key3: true, key4: null };
        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectObj);
      });

      test('2 level depth with attributes and one relationships', () => {
        const obj = {
          data: {
            id: 1,
            type: 'user',
            attributes: { key1: 1, key2: '2', key3: true, key4: null },
            relationships: {
              order: {
                data: {
                  id: 1,
                  type: 'order',
                },
              },
            },
          },
          included: [
            {
              id: 1,
              type: 'order',
              attributes: { field1: 'field2', field2: 'field2' },
            },
          ],
        };
        const expectObj = {
          id: 1,
          type: 'user',
          key1: 1,
          key2: '2',
          key3: true,
          key4: null,
          order: {
            id: 1,
            type: 'order',
            field1: 'field2',
            field2: 'field2',
          },
        };
        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectObj);
      });

      test('2 level depth with attributes and relationships which include array entities', () => {
        const obj = {
          data: {
            id: 1,
            type: 'user',
            attributes: { key1: 1, key2: '2', key3: true, key4: null },
            relationships: {
              orders: {
                data: [
                  {
                    id: 1,
                    type: 'order',
                  },
                  {
                    id: 2,
                    type: 'order',
                  },
                ],
              },
            },
          },
          included: [
            {
              id: 1,
              type: 'order',
              attributes: { field1: 'order1', field2: 'order1' },
            },
            {
              id: 2,
              type: 'order',
              attributes: { field1: 'order2', field2: 'order2' },
            },
          ],
        };
        const expectObj = {
          id: 1,
          type: 'user',
          key1: 1,
          key2: '2',
          key3: true,
          key4: null,
          orders: [
            {
              id: 1,
              type: 'order',
              field1: 'order1',
              field2: 'order1',
            },
            {
              id: 2,
              type: 'order',
              field1: 'order2',
              field2: 'order2',
            },
          ],
        };
        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectObj);
      });

      test('2 level depth with attributes and relationships which include array entities with broken links', () => {
        const obj = {
          data: {
            id: 1,
            type: 'user',
            attributes: { key1: 1, key2: '2', key3: true, key4: null },
            relationships: {
              orders: {
                data: [
                  {
                    id: 1,
                    type: 'order',
                  },
                  {
                    id: 2,
                    type: 'order',
                  },
                ],
              },
            },
          },
          included: [
            {
              id: 1,
              type: 'order',
              attributes: { field1: 'order1', field2: 'order1' },
            },
          ],
        };
        const expectObj = {
          id: 1,
          type: 'user',
          key1: 1,
          key2: '2',
          key3: true,
          key4: null,
          orders: [
            {
              id: 1,
              type: 'order',
              field1: 'order1',
              field2: 'order1',
            },
            {
              id: 2,
              type: 'order',
            },
          ],
        };
        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectObj);
      });

      test('2 level depth and array entities', () => {
        const obj = {
          data: [
            {
              id: 1,
              type: 'user',
              attributes: { key1: 1, key2: '1', key3: true, key4: null },
            },
            {
              id: 2,
              type: 'user',
              attributes: { key1: 2, key2: '2', key3: false, key4: undefined },
            },
          ],
        };
        const expectObj = [
          {
            id: 1,
            type: 'user',
            key1: 1,
            key2: '1',
            key3: true,
            key4: null,
          },
          {
            id: 2,
            type: 'user',
            key1: 2,
            key2: '2',
            key3: false,
            key4: undefined,
          },
        ];
        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectObj);
      });

      test('2 level depth and array entities with the same relationships', () => {
        const obj = {
          data: [
            {
              id: 1,
              type: 'user',
              attributes: { key1: 1, key2: '1', key3: true, key4: null },
              relationships: {
                order: {
                  data: {
                    id: 1,
                    type: 'order',
                  },
                },
              },
            },
            {
              id: 2,
              type: 'user',
              attributes: { key1: 2, key2: '2', key3: false, key4: undefined },
              relationships: {
                order: {
                  data: {
                    id: 1,
                    type: 'order',
                  },
                },
              },
            },
          ],
          included: [
            {
              id: 1,
              type: 'order',
              attributes: { field1: 'order1', field2: 'order1' },
            },
          ],
        };
        const expectObj = [
          {
            id: 1,
            type: 'user',
            key1: 1,
            key2: '1',
            key3: true,
            key4: null,
            order: {
              field1: 'order1',
              field2: 'order1',
              id: 1,
              type: 'order',
            },
          },
          {
            id: 2,
            type: 'user',
            key1: 2,
            key2: '2',
            key3: false,
            key4: undefined,
            order: {
              field1: 'order1',
              field2: 'order1',
              id: 1,
              type: 'order',
            },
          },
        ];
        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectObj);
      });

      test('3 level depth', () => {
        const obj = {
          data: {
            id: 1,
            type: 'user',
            attributes: { key1: 1, key2: '2', key3: true, key4: null },
            relationships: {
              order: {
                data: {
                  id: 1,
                  type: 'order',
                },
              },
            },
          },
          included: [
            {
              id: 1,
              type: 'order',
              attributes: {
                anyKey: 'any',
              },
              relationships: {
                payment: {
                  data: {
                    id: 1,
                    type: 'payment',
                  },
                },
              },
            },
            {
              id: 1,
              type: 'payment',
              attributes: {
                anyKey: 'any',
              },
            },
          ],
        };
        const expectObj = {
          id: 1,
          type: 'user',
          key1: 1,
          key2: '2',
          key3: true,
          key4: null,
          order: {
            id: 1,
            type: 'order',
            anyKey: 'any',
            payment: {
              id: 1,
              type: 'payment',
              anyKey: 'any',
            },
          },
        };
        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectObj);
      });

      test('3 level depth with the same type and different id', () => {
        const obj = {
          data: {
            id: 1,
            type: 'user',
            attributes: {
              key1: 1,
              key2: '2',
              key3: true,
              key4: null,
            },
            relationships: {
              order: {
                data: {
                  id: 1,
                  type: 'order',
                },
              },
            },
          },
          included: [
            {
              id: 1,
              type: 'order',
              attributes: {
                anyKey: 'any',
              },
              relationships: {
                user: { data: { id: 2, type: 'user' } },
              },
            },
            { id: 2, type: 'user', attributes: { anyKey: 'any' } },
          ],
        };

        const expectObj = {
          id: 1,
          type: 'user',
          key1: 1,
          key2: '2',
          key3: true,
          key4: null,
          order: {
            id: 1,
            type: 'order',
            anyKey: 'any',
            user: { id: 2, type: 'user', anyKey: 'any' },
          },
        };
        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectObj);
      });

      test('3 level depth with circular dependency', () => {
        const obj = {
          data: {
            id: 1,
            type: 'user',
            attributes: {
              key1: 1,
              key2: '2',
              key3: true,
              key4: null,
            },
            relationships: {
              order: {
                data: {
                  id: 1,
                  type: 'order',
                },
              },
            },
          },
          included: [
            {
              id: 1,
              type: 'order',
              attributes: {
                anyKey: 'any',
              },
              relationships: {
                user: {
                  data: {
                    id: 1,
                    type: 'user',
                  },
                },
              },
            },
          ],
        };

        const expectObj = {
          id: 1,
          type: 'user',
          key1: 1,
          key2: '2',
          key3: true,
          key4: null,
          order: {
            id: 1,
            type: 'order',
            anyKey: 'any',
            user: { id: 1, type: 'user' },
          },
        };
        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectObj);
      });

      test('3 level depth with array data', () => {
        const obj = {
          data: {
            id: 1,
            type: 'user',
            attributes: {
              key1: 1,
              key2: '2',
              key3: true,
              key4: null,
            },
            relationships: {
              order: {
                data: {
                  id: 1,
                  type: 'order',
                },
              },
            },
          },
          included: [
            {
              id: 1,
              type: 'order',
              attributes: {
                anyKey: 'any',
              },
              relationships: {
                payments: {
                  data: [
                    { id: 1, type: 'payment' },
                    { id: 2, type: 'payment' },
                  ],
                },
              },
            },
            { id: 1, type: 'payment', attributes: { anyKey: 'any' } },
            { id: 2, type: 'payment', attributes: { anyKey: 'any' } },
          ],
        };

        const expectObj = {
          id: 1,
          type: 'user',
          key1: 1,
          key2: '2',
          key3: true,
          key4: null,
          order: {
            id: 1,
            type: 'order',
            anyKey: 'any',
            payments: [
              { id: 1, type: 'payment', anyKey: 'any' },
              { id: 2, type: 'payment', anyKey: 'any' },
            ],
          },
        };
        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectObj);
      });

      test('3 level depth with array data and the same type and different id', () => {
        const obj = {
          data: {
            id: 1,
            type: 'user',
            attributes: {
              key1: 1,
              key2: '2',
              key3: true,
              key4: null,
            },
            relationships: {
              order: {
                data: {
                  id: 1,
                  type: 'order',
                },
              },
            },
          },
          included: [
            {
              id: 1,
              type: 'order',
              attributes: {
                anyKey: 'any',
              },
              relationships: {
                users: {
                  data: [
                    { id: 2, type: 'user' },
                    { id: 3, type: 'user' },
                  ],
                },
              },
            },
            { id: 2, type: 'user', attributes: { anyKey: 'any' } },
            { id: 3, type: 'user', attributes: { anyKey: 'any' } },
          ],
        };

        const expectObj = {
          id: 1,
          type: 'user',
          key1: 1,
          key2: '2',
          key3: true,
          key4: null,
          order: {
            id: 1,
            type: 'order',
            anyKey: 'any',
            users: [
              { id: 2, type: 'user', anyKey: 'any' },
              { id: 3, type: 'user', anyKey: 'any' },
            ],
          },
        };
        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectObj);
      });

      test('3 level depth with array data and circular dependencies', () => {
        const obj = {
          data: {
            id: 1,
            type: 'user',
            attributes: {
              key1: 1,
              key2: '2',
              key3: true,
              key4: null,
            },
            relationships: {
              order: {
                data: {
                  id: 1,
                  type: 'order',
                },
              },
            },
          },
          included: [
            {
              id: 1,
              type: 'order',
              attributes: {
                anyKey: 'any',
              },
              relationships: {
                users: {
                  data: [
                    { id: 1, type: 'user' },
                    { id: 2, type: 'user' },
                  ],
                },
              },
            },
            {
              id: 2,
              type: 'user',
              attributes: {
                anyKey: 'any',
                otherField: 'any',
              },
            },
          ],
        };

        const expectObj = {
          id: 1,
          type: 'user',
          key1: 1,
          key2: '2',
          key3: true,
          key4: null,
          order: {
            id: 1,
            type: 'order',
            anyKey: 'any',
            users: [
              { id: 1, type: 'user' },
              { id: 2, type: 'user', anyKey: 'any', otherField: 'any' },
            ],
          },
        };
        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectObj);
      });

      test('2 level depth with circular itself', () => {
        const obj = {
          data: {
            id: 1,
            type: 'user',
            attributes: {
              key1: 1,
              key2: '2',
              key3: true,
              key4: null,
            },
            relationships: {
              user: {
                data: {
                  id: 1,
                  type: 'user',
                },
              },
            },
          },
        };

        const expectObj = {
          id: 1,
          type: 'user',
          key1: 1,
          key2: '2',
          key3: true,
          key4: null,
          user: {
            id: 1,
            type: 'user',
          },
        };
        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectObj);
      });

      test('3 level depth with circular itself', () => {
        const obj = {
          data: {
            id: 1,
            type: 'user',
            attributes: {
              key1: 1,
              key2: '2',
              key3: true,
              key4: null,
            },
            relationships: {
              order: {
                data: {
                  id: 1,
                  type: 'order',
                },
              },
            },
          },
          included: [
            {
              id: 1,
              type: 'order',
              attributes: {
                anyKey: 'any',
              },
              relationships: {
                user: {
                  data: {
                    id: 1,
                    type: 'user',
                  },
                },
              },
            },
          ],
        };

        const expectObj = {
          id: 1,
          type: 'user',
          key1: 1,
          key2: '2',
          key3: true,
          key4: null,
          order: {
            id: 1,
            type: 'order',
            anyKey: 'any',
            user: { id: 1, type: 'user' },
          },
        };
        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectObj);
      });
    });

    describe('array', () => {
      test('array with 2 items and 1 level depth', () => {
        const obj = {
          data: [
            {
              id: 1,
              type: 'user',
              attributes: {
                key1: 1,
                key2: '2',
                key3: true,
                key4: null,
              },
            },

            {
              id: 2,
              type: 'user',
              attributes: {
                key1: 2,
                key2: '3',
                key3: false,
                key4: undefined,
              },
            },
          ],
        };

        const expectObj = [
          {
            id: 1,
            type: 'user',
            key1: 1,
            key2: '2',
            key3: true,
            key4: null,
          },

          {
            id: 2,
            type: 'user',
            key1: 2,
            key2: '3',
            key3: false,
            key4: undefined,
          },
        ];

        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectObj);
      });

      test('array with 2 items and 2 level depth', () => {
        const obj = {
          data: [
            {
              id: 1,
              type: 'user',
              attributes: {
                key1: 1,
                key2: '2',
                key3: true,
                key4: null,
              },
              relationships: {
                order: {
                  data: {
                    id: 1,
                    type: 'order',
                  },
                },
              },
            },

            {
              id: 2,
              type: 'user',
              attributes: {
                key1: 2,
                key2: '3',
                key3: false,
                key4: undefined,
              },
              relationships: {
                order: {
                  data: {
                    id: 1,
                    type: 'order',
                  },
                },
              },
            },
          ],
          included: [
            {
              id: 1,
              type: 'order',
              attributes: {
                anyKey: 'any',
              },
            },
          ],
        };

        const expectObj = [
          {
            id: 1,
            type: 'user',
            key1: 1,
            key2: '2',
            key3: true,
            key4: null,
            order: {
              id: 1,
              type: 'order',
              anyKey: 'any',
            },
          },

          {
            id: 2,
            type: 'user',
            key1: 2,
            key2: '3',
            key3: false,
            key4: undefined,
            order: {
              id: 1,
              type: 'order',
              anyKey: 'any',
            },
          },
        ];

        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectObj);
      });

      test('array with 2 items and 3 level depth', () => {
        const obj = {
          data: [
            {
              id: 1,
              type: 'user',
              attributes: {
                key1: 1,
                key2: '2',
                key3: true,
                key4: null,
              },
              relationships: {
                order: {
                  data: {
                    id: 1,
                    type: 'order',
                  },
                },
              },
            },

            {
              id: 2,
              type: 'user',
              attributes: {
                key1: 2,
                key2: '3',
                key3: false,
                key4: undefined,
              },
              relationships: {
                order: {
                  data: {
                    id: 1,
                    type: 'order',
                  },
                },
              },
            },
          ],
          included: [
            {
              id: 1,
              type: 'order',
              attributes: {
                anyKey: 'any',
              },
              relationships: {
                payment: { data: { id: 1, type: 'payment' } },
              },
            },
            { id: 1, type: 'payment', attributes: { anyKey: 'any' } },
          ],
        };

        const expectObj = [
          {
            id: 1,
            type: 'user',
            key1: 1,
            key2: '2',
            key3: true,
            key4: null,
            order: {
              id: 1,
              type: 'order',
              anyKey: 'any',
              payment: { id: 1, type: 'payment', anyKey: 'any' },
            },
          },

          {
            id: 2,
            type: 'user',
            key1: 2,
            key2: '3',
            key3: false,
            key4: undefined,
            order: {
              id: 1,
              type: 'order',
              anyKey: 'any',
              payment: { id: 1, type: 'payment', anyKey: 'any' },
            },
          },
        ];

        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectObj);
      });

      test('array with 2 items and 3 level depth and different object in different places', () => {
        const obj = {
          data: [
            {
              id: 1,
              type: 'user',
              attributes: {
                key1: 1,
                key2: '2',
                key3: true,
                key4: null,
              },
              relationships: {
                order: {
                  data: {
                    id: 1,
                    type: 'order',
                  },
                },
              },
            },
            {
              id: 2,
              type: 'user',
              attributes: {
                key1: 2,
                key2: '3',
                key3: false,
                key4: undefined,
              },
              relationships: {
                order: {
                  data: {
                    id: 2,
                    type: 'order',
                  },
                },
              },
            },
          ],
          included: [
            {
              id: 1,
              type: 'order',
              attributes: {
                anyKey: 'any',
              },
              relationships: {
                payment: { data: { id: 1, type: 'payment' } },
                user: { data: { id: 2, type: 'user' } },
              },
            },
            { id: 1, type: 'payment', attributes: { anyKey: 'any' } },
            {
              id: 2,
              type: 'order',
              attributes: {
                anyKey: 'any',
              },
              relationships: {
                payment: { data: { id: 2, type: 'payment' } },
                user: { data: { id: 1, type: 'user' } },
              },
            },
            { id: 2, type: 'payment', attributes: { anyKey: 'any' } },
          ],
        };
        const expectedObj = [
          {
            id: 1,
            type: 'user',
            key1: 1,
            key2: '2',
            key3: true,
            key4: null,
            order: {
              id: 1,
              type: 'order',
              anyKey: 'any',
              payment: { id: 1, type: 'payment', anyKey: 'any' },
              user: {
                id: 2,
                type: 'user',
                key1: 2,
                key2: '3',
                key3: false,
                key4: undefined,
                order: {
                  id: 2,
                  type: 'order',
                  anyKey: 'any',
                  payment: { id: 2, type: 'payment', anyKey: 'any' },
                  user: { id: 1, type: 'user' },
                },
              },
            },
          },
          {
            id: 2,
            type: 'user',
            key1: 2,
            key2: '3',
            key3: false,
            key4: undefined,
            order: {
              id: 2,
              type: 'order',
              anyKey: 'any',
              payment: { id: 2, type: 'payment', anyKey: 'any' },
              user: {
                id: 1,
                type: 'user',
                key1: 1,
                key2: '2',
                key3: true,
                key4: null,
                order: {
                  id: 1,
                  type: 'order',
                  anyKey: 'any',
                  payment: { id: 1, type: 'payment', anyKey: 'any' },
                  user: {
                    id: 2,
                    type: 'user',
                  },
                },
              },
            },
          },
        ];

        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectedObj);
      });

      test('array with 2 items and 3 level depth and circular dependency in another index array', () => {
        const obj = {
          data: [
            {
              id: 1,
              type: 'user',
              attributes: {
                key1: 1,
                key2: '2',
                key3: true,
                key4: null,
              },
              relationships: {
                order: {
                  data: {
                    id: 1,
                    type: 'order',
                  },
                },
              },
            },
            {
              id: 2,
              type: 'user',
              attributes: {
                key1: 2,
                key2: '3',
                key3: false,
                key4: undefined,
              },
              relationships: {
                order: {
                  data: {
                    id: 2,
                    type: 'order',
                  },
                },
              },
            },
          ],
          included: [
            {
              id: 1,
              type: 'order',
              attributes: {
                anyKey: 'any',
              },
              relationships: {
                user: {
                  data: {
                    id: 2,
                    type: 'user',
                  },
                },
              },
            },
            {
              id: 2,
              type: 'order',
              attributes: {
                anyKey: 'any',
              },
            },
          ],
        };
        const expectedObj = [
          {
            id: 1,
            type: 'user',
            key1: 1,
            key2: '2',
            key3: true,
            key4: null,
            order: {
              id: 1,
              type: 'order',
              anyKey: 'any',
              user: {
                id: 2,
                type: 'user',
                key1: 2,
                key2: '3',
                key3: false,
                key4: undefined,
                order: {
                  id: 2,
                  type: 'order',
                  anyKey: 'any',
                },
              },
            },
          },
          {
            id: 2,
            type: 'user',
            key1: 2,
            key2: '3',
            key3: false,
            key4: undefined,
            order: {
              id: 2,
              type: 'order',
              anyKey: 'any',
            },
          },
        ];

        const data = jsonApiDeserialize(obj);
        expect(data).toEqual(expectedObj);
      });
    });
  });
});
