type JsonApiRawData = {
  data?: JsonApiData | JsonApiData[];
  included?: JsonApiData[];
};

interface JsonApiData {
  id: ID;
  type: string;
  attributes?: Record<string, any>;
  meta?: Record<string, any>;
  relationships?: {
    [relationName: string]: {
      data: JsonApiData | JsonApiData[];
      meta?: Record<string, any>;
    };
  };
}

interface JsonApiModel {
  id: ID;
  type: string;
  [key: string]: any;
}

const isJsonApiModel = (obj: any): obj is JsonApiModel => obj.id && obj.type;

const isJsonApiRawModel = (obj: any): obj is JsonApiRawData => obj.data;

const createHashModel = ({ id, type }: { id: ID; type: string }) => `${type}{${id}}`;

const createCache = ({ data, included }: JsonApiRawData) => {
  const cache = new Map<string, JsonApiData>();
  if (Array.isArray(included)) {
    included.filter(isJsonApiModel).forEach(item => {
      cache.set(createHashModel(item), item);
    });
  }

  if (Array.isArray(data)) {
    data.filter(isJsonApiModel).forEach(item => {
      cache.set(createHashModel(item), item);
    });
  } else if (isJsonApiModel(data)) {
    cache.set(createHashModel(data), data);
  }

  return cache;
};

function circularDeserialize(
  data: JsonApiData | JsonApiData[],
  { depth, cache }: { depth: string; cache: Map<string, JsonApiData> },
): any {
  if (Array.isArray(data)) {
    return data.map(
      (item, index) => circularDeserialize(item, { cache, depth: `${depth}.[${index}]` }),
      [],
    );
  }
  const hash = createHashModel(data);
  if (depth.includes(hash)) {
    return {
      id: data.id,
      type: data.type,
    };
  }

  const cachedData = cache.get(hash);
  if (cachedData) {
    let model: JsonApiModel = {
      id: cachedData.id,
      type: cachedData.type,
    };

    if (cachedData.attributes) {
      model = { ...model, ...cachedData.attributes };
    }

    if (cachedData.relationships) {
      Object.keys(cachedData.relationships).forEach(modelName => {
        if (cachedData.relationships && cachedData.relationships[modelName].data) {
          model[modelName] = circularDeserialize(cachedData.relationships[modelName].data, {
            cache,
            depth: `${depth}.${hash}`,
          });
        }
      });
    }

    return model;
  }

  return data;
}

export function jsonApiDeserialize(data: unknown): any {
  if (data && isJsonApiRawModel(data)) {
    const cache = createCache(data);

    if (Array.isArray(data.data)) {
      return data.data
        .filter(isJsonApiModel)
        .map((item: JsonApiData) => circularDeserialize(item, { depth: '', cache }));
    } else if (isJsonApiModel(data.data)) {
      return circularDeserialize(data.data, { depth: '', cache });
    }
  }

  return data;
}
