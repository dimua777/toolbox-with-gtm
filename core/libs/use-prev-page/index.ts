import { useEffect, useState } from 'react';
import { usePrevious } from 'react-use';
import { useRouter } from 'next/router';
import constate from 'constate';

const usePrevPageImpl = () => {
  const { pathname, asPath } = useRouter();
  const prevPathname = usePrevious(pathname);
  const prevAsPath = usePrevious(asPath);

  const [prevPage, setPrevPage] = useState<string | undefined>();

  useEffect(() => {
    if (prevPathname && prevPathname !== pathname) {
      setPrevPage(prevAsPath);
    }
  }, [prevAsPath, pathname, prevPathname]);

  return { prevPage: prevPage || '/', isFallback: !prevPage };
};

export const [PrevPageProvider, usePrevPage] = constate(usePrevPageImpl);
