/* eslint-disable */
import { QueryCache, QueryClient, QueryClientProvider, useQuery } from 'react-query';
import { renderHook } from '@testing-library/react-hooks';
import { addServerState } from './helpers';

import { ReactQueryProvider } from './provider';

describe('ReactQueryProvider', () => {
  let queryCache: QueryCache;
  let queryClient: QueryClient;
  let mockFetcher: jest.Mock;

  beforeEach(() => {
    mockFetcher = jest.fn(() => Promise.resolve('any text'));
    queryCache = new QueryCache();
    queryClient = new QueryClient({
      defaultOptions: {
        queries: {
          queryFn: mockFetcher,
        },
      },
      queryCache,
    });
  });

  test('to pass query client to the provider with prefetching data', async () => {
    const useTestQuery = () => {
      const { data } = useQuery(['/api/test']);
      return data;
    };

    const TestComponent = () => {
      useTestQuery();
      return null;
    };

    TestComponent.getInitialProps = async () => {
      await queryClient.fetchQuery(['/api/test']);
      return addServerState(queryClient);
    };

    const wrapper = ({ children, pageProps }: any) => (
      <ReactQueryProvider pageProps={pageProps}>{children}</ReactQueryProvider>
    );

    const props = await TestComponent.getInitialProps();

    expect(mockFetcher).toHaveBeenCalled();
    expect(mockFetcher).toHaveBeenCalledTimes(1);
    expect(mockFetcher).toHaveBeenCalledWith({ pageParams: undefined, queryKey: ['/api/test'] });

    const { result, waitForNextUpdate } = renderHook(() => useTestQuery(), {
      wrapper,
      initialProps: {
        pageProps: props,
      },
    });

    await waitForNextUpdate();

    expect(result.current).toEqual('any text');
  });
});
