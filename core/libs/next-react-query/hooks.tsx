import { useCallback } from 'react';
import {
  InvalidateQueryFilters,
  MutationFunction,
  useMutation as useMutationReactQuery,
  UseMutationOptions,
  useQueryClient,
} from 'react-query';

export const useQueryInvalidate = () => {
  const queryClient = useQueryClient();

  return useCallback(
    async (entities: BackendEntity[], filter?: InvalidateQueryFilters) => {
      if (entities.length > 0) {
        await queryClient.invalidateQueries({
          predicate: query => {
            if (Array.isArray(query.queryKey[2])) {
              return query.queryKey[2].some(tag => entities.includes(tag));
            }

            return false;
          },
          ...filter,
        });
      }
    },
    [queryClient],
  );
};

export function useMutationWithQueryInvalidate<
  TData = unknown,
  TError = unknown,
  TVariables = void,
  TContext = unknown,
>(
  mutationFn: MutationFunction<TData, TVariables>,
  options: UseMutationOptions<TData, TError, TVariables, TContext> & {
    invalidateEntities?: BackendEntity[];
  } = {},
) {
  const queryInvalidate = useQueryInvalidate();
  const { onSuccess, invalidateEntities, ...otherOptions } = options;
  return useMutationReactQuery<TData, TError, TVariables, TContext>(mutationFn, {
    ...otherOptions,
    onSuccess: async (data, variables, context) => {
      if (Array.isArray(invalidateEntities) && invalidateEntities.length > 0) {
        await queryInvalidate(invalidateEntities);
      }
      if (onSuccess) {
        await onSuccess(data, variables, context);
      }
    },
  });
}
