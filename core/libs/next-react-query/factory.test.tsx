import { useInfiniteQuery, useQuery } from 'react-query';
import { renderHook } from '@testing-library/react-hooks';

import { createUseInfiniteQueryHook, createUseQueryHook } from './factory';
import { GetQueryKey } from './fetcher';

jest.mock('react-query', () => ({
  useQuery: jest.fn(),
  useInfiniteQuery: jest.fn(),
}));

jest.mock('../api', () => {
  return {
    apiClient: {
      get: jest.fn(),
    },
    createApiClient: () => ({
      get: jest.fn(),
    }),
  };
});

const mockFetcher = jest.fn(() => Promise.resolve({ anyField: true }));

describe('createUseQueryHook', () => {
  test('creates hook that accepts key params and useQuery options', () => {
    const getUsersQueryKey: GetQueryKey<{ id: string }> = params => ['/users[id]', params];

    const defaults = { initialData: [] };
    const useUsers = createUseQueryHook<any>(defaults)(getUsersQueryKey);

    const options = { cacheTime: 300, queryFn: mockFetcher };
    const keyParams = { id: 'id' };
    renderHook(() => useUsers(keyParams, options));
    expect(useQuery).toBeCalledWith(getUsersQueryKey(keyParams), { ...defaults, ...options });
  });
});

describe('createUseInfiniteQueryHook', () => {
  test('creates hook that accepts key params and useQuery options', () => {
    const getUsersQueryKey: GetQueryKey<{ order: string }> = params => ['/users', params];

    const useUsers = createUseInfiniteQueryHook<any>()(getUsersQueryKey);

    const options = { cacheTime: 300, queryFn: mockFetcher };
    const keyParams = { order: 'asc' };
    renderHook(() => useUsers(keyParams, options));
    expect(useInfiniteQuery).toBeCalledWith(getUsersQueryKey(keyParams), options);
  });
});
