import { QueryFunction, QueryFunctionContext } from 'react-query';
import { NextPageContext } from 'next';
import { AxiosRequestConfig } from 'axios';

import { apiClient, createApiClient } from '../api';

type QueryKeyParams = Record<string, any>;
type QueryKey = [string] | [string, QueryKeyParams] | [string, QueryKeyParams, BackendEntity[]];

/** first array element should be Next.js like url: /user/[id] */
export type GetQueryKey<P = undefined> = P extends undefined
  ? () => QueryKey
  : (params: P) => QueryKey;

/**
 *
 * @param queryKey: QueryKey
 * @param pageParam: next/prev page cursor
 * you should return object from `getNextPageParam` or `getPreviousPageParam` for `useInfiniteQuery`
 */
const buildQueryKey = (queryKey: QueryKey, pageParam: unknown) => {
  const pageParams = typeof pageParam === 'object' ? pageParam : {};
  const params = { ...queryKey[1], ...pageParams };
  let url = queryKey[0];

  Object.entries(params).forEach(([key, val]) => {
    const keyRegExp = new RegExp(`\\[${key}\\]`, 'g');

    if (url.search(keyRegExp) >= 0) {
      delete params[key];
      url = url.replace(new RegExp(keyRegExp), val);
    }
  });

  return { url, params };
};

export const createQueryFetcher = (
  ctx?: NextPageContext,
  config?: AxiosRequestConfig,
): QueryFunction => {
  const client = ctx?.req ? createApiClient(ctx) : apiClient;

  return <T>(context: any) => {
    // TODO: fix in future if possible. RQ types such stable...
    const { queryKey, pageParam } = context as QueryFunctionContext<QueryKey>;

    const { url, params } = buildQueryKey(queryKey, pageParam);
    return client.get<T>(url, { ...config, params }).then(res => res.data);
  };
};
