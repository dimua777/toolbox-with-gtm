import { FC, useRef } from 'react';
import {
  DefaultOptions,
  Hydrate,
  MutationCache,
  QueryCache,
  QueryClient,
  QueryClientProvider,
  setLogger,
} from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';

import { nope } from '../nope';
import { defaultConfig } from './config';
import { STATE_PROP_NAME } from './helpers';

interface ReactQueryProviderProps {
  config?: {
    queryCache?: QueryCache;
    mutationCache?: MutationCache;
    defaultOptions?: DefaultOptions;
  };
  pageProps: any;
}

/**
 * Disable logging errors in console
 * api lib responsible for this
 */
setLogger({
  log: nope,
  warn: nope,
  error: nope,
});

export const ReactQueryProvider: FC<ReactQueryProviderProps> = ({
  config = defaultConfig,
  children,
  pageProps,
}) => {
  const dehydratedState = pageProps?.[STATE_PROP_NAME] ?? pageProps?.props?.[STATE_PROP_NAME];
  const client = useRef<QueryClient>(null as any);
  if (!client.current) {
    client.current = new QueryClient(config as any);
  }

  return (
    <QueryClientProvider client={client.current}>
      <ReactQueryDevtools initialIsOpen={false} />
      <Hydrate state={dehydratedState}>{children}</Hydrate>
    </QueryClientProvider>
  );
};
