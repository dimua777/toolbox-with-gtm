import { DefaultOptions } from 'react-query';

import { redirect } from '../redirect';
import { createQueryFetcher } from './fetcher';
import { getInfiniteQueryNextPageParam } from './helpers';

export const defaultConfig: { defaultOptions: DefaultOptions } = {
  defaultOptions: {
    queries: {
      queryFn: createQueryFetcher(),
      onError: error => {
        const err = error as { statusCode: number };
        if (err && err.statusCode === 401 && !redirect.clientRedirectPending) {
          redirect('/sign-in');
        }
      },
      getNextPageParam: getInfiniteQueryNextPageParam as any,
      notifyOnChangeProps: 'tracked',
    },
  },
};
