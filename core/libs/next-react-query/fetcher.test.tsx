/* eslint-disable */
import { apiClient } from '../api';
import { createQueryFetcher } from './fetcher';

jest.mock('../api', () => {
  return {
    apiClient: {
      get: jest.fn(),
    },
    createApiClient: () => ({
      get: jest.fn(),
    }),
  };
});

describe('createQueryFetcher', () => {
  test('formatting simple url', async () => {
    (apiClient.get as jest.Mock).mockReset();
    (apiClient.get as jest.Mock).mockImplementation(() => Promise.resolve({ data: 'test' }));

    const queryKey = ['/api/fetch'];

    const callback = createQueryFetcher();
    const result = await callback({ queryKey });

    expect(result).toBe('test');
    expect(apiClient.get as jest.Mock).toHaveBeenCalled();
    expect(apiClient.get as jest.Mock).toHaveBeenCalledWith('/api/fetch', { params: {} });
  });

  test('formatting url with arguments', async () => {
    (apiClient.get as jest.Mock).mockReset();
    (apiClient.get as jest.Mock).mockImplementation(() => Promise.resolve({ data: 'test' }));

    const queryKey = ['/api/fetch/[id]/client/[orderNumber5]', { id: 555, orderNumber5: 'text' }];

    const callback = createQueryFetcher();
    const result = await callback({ queryKey });

    expect(result).toBe('test');
    expect(apiClient.get as jest.Mock).toHaveBeenCalled();
    expect(apiClient.get as jest.Mock).toHaveBeenCalledWith('/api/fetch/555/client/text', {
      params: {},
    });
  });

  test('formatting url with query params', async () => {
    (apiClient.get as jest.Mock).mockReset();
    (apiClient.get as jest.Mock).mockImplementation(() => Promise.resolve({ data: 'test' }));

    const queryKey = [
      '/api/fetch',
      {
        query1: 'text',
        query2: 10,
      },
    ];

    const callback = createQueryFetcher();
    const result = await callback({ queryKey });

    expect(result).toBe('test');
    expect(apiClient.get as jest.Mock).toHaveBeenCalled();
    expect(apiClient.get as jest.Mock).toHaveBeenCalledWith('/api/fetch', {
      params: { query1: 'text', query2: 10 },
    });
  });

  test('formatting url with arguments and query params', async () => {
    (apiClient.get as jest.Mock).mockReset();
    (apiClient.get as jest.Mock).mockImplementation(() => Promise.resolve({ data: 'test' }));

    const queryKey = [
      '/api/fetch/[id]/[clientNumber]',
      {
        query1: 'text',
        query2: 100,
        id: 10,
        clientNumber: 1000000,
      },
    ];

    const callback = createQueryFetcher();
    const result = await callback({ queryKey });

    expect(result).toBe('test');
    expect(apiClient.get as jest.Mock).toHaveBeenCalled();
    expect(apiClient.get as jest.Mock).toHaveBeenCalledWith('/api/fetch/10/1000000', {
      params: { query1: 'text', query2: 100 },
    });
  });

  test('formatting url and additional config for api client', async () => {
    (apiClient.get as jest.Mock).mockReset();
    (apiClient.get as jest.Mock).mockImplementation(() => Promise.resolve({ data: 'test' }));

    const queryKey = ['/api/fetch'];

    const callback = createQueryFetcher(undefined, { extra1: 'any text' });
    const result = await callback({ queryKey });

    expect(result).toBe('test');
    expect(apiClient.get as jest.Mock).toHaveBeenCalled();
    expect(apiClient.get as jest.Mock).toHaveBeenCalledWith('/api/fetch', {
      extra1: 'any text',
      params: {},
    });
  });

  test.todo('Need to test with to pass pageParams from infinite scroll');
});
