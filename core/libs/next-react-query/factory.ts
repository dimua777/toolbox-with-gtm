import { useInfiniteQuery, UseInfiniteQueryOptions, useQuery, UseQueryOptions } from 'react-query';
import merge from 'deepmerge';

import { GetQueryKey } from './fetcher';

export function createUseQueryHook<TQueryFnData = unknown, TQueryFnError = unknown>(
  factoryOptions?: UseQueryOptions<TQueryFnData, TQueryFnError>,
) {
  return <QueryKeyParamsType>(getQueryKey: GetQueryKey<QueryKeyParamsType>) =>
    (keyParams: QueryKeyParamsType, options?: UseQueryOptions<TQueryFnData, TQueryFnError>) => {
      return useQuery<TQueryFnData, TQueryFnError>(
        getQueryKey(keyParams),
        merge(factoryOptions as any, options as any),
      );
    };
}

export function createUseInfiniteQueryHook<TQueryFnData = unknown, TQueryFnError = unknown>(
  factoryOptions?: UseInfiniteQueryOptions<TQueryFnData, TQueryFnError>,
) {
  return <QueryKeyParamsType>(getQueryKey: GetQueryKey<QueryKeyParamsType>) =>
    (
      keyParams?: QueryKeyParamsType,
      options?: UseInfiniteQueryOptions<TQueryFnData, TQueryFnError>,
    ) => {
      return useInfiniteQuery<TQueryFnData, TQueryFnError>(
        getQueryKey(keyParams as any),
        merge(factoryOptions as any, options as any),
      );
    };
}
