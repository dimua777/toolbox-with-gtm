/* eslint-disable */
import { QueryCache, QueryClient, QueryClientProvider, useQuery } from 'react-query';
import { act, renderHook, WrapperComponent } from '@testing-library/react-hooks';

import { useMutationWithQueryInvalidate, useQueryInvalidate } from './hooks';

describe('useQueryInvalidate', () => {
  let queryCache: QueryCache;
  let queryClient: QueryClient;
  let mockFetcher: jest.Mock;

  beforeEach(() => {
    mockFetcher = jest.fn(() => Promise.resolve({ anyField: true }));
    queryCache = new QueryCache();
    queryClient = new QueryClient({
      defaultOptions: {
        queries: {
          queryFn: mockFetcher,
        },
      },
      queryCache,
    });
  });

  test('clear existing tag', async () => {
    const mockFetcher = jest.fn((_: { queryKey: any[] }) => Promise.resolve({ anyField: true }));
    queryCache = new QueryCache();
    queryClient = new QueryClient({
      defaultOptions: {
        queries: {
          queryFn: mockFetcher,
        },
      },
      queryCache,
    });

    const wrapper: WrapperComponent<any> = ({ children }) => {
      return <QueryClientProvider client={queryClient}> {children} </QueryClientProvider>;
    };

    const key1 = ['api/url', { param: 1 }, ['tag1']];
    const { result: data1, waitFor } = renderHook(() => useQuery(key1), { wrapper });

    await waitFor(() => !!data1.current.data);

    const key2 = ['api/url2', { param: 2 }, ['tag1', 'tag2']];
    const { result: data2 } = renderHook(() => useQuery(key2), { wrapper });

    await waitFor(() => !!data2.current.data);

    const key3 = ['api/url3', { param: 3 }, ['tag2']];
    const { result: data3 } = renderHook(() => useQuery(key3), {
      wrapper,
    });

    await waitFor(() => !!data3.current.data);

    expect(data1.current.data).toEqual({ anyField: true });
    expect(data2.current.data).toEqual({ anyField: true });
    expect(data3.current.data).toEqual({ anyField: true });
    expect(mockFetcher).toHaveBeenCalled();
    expect(mockFetcher).toHaveBeenCalledTimes(3);
    expect(mockFetcher.mock.calls[0][0].queryKey).toEqual(key1);
    expect(mockFetcher.mock.calls[1][0].queryKey).toEqual(key2);
    expect(mockFetcher.mock.calls[2][0].queryKey).toEqual(key3);

    const { result } = renderHook(() => useQueryInvalidate(), { wrapper });

    await act(() =>
      result.current(['tag1'], {
        refetchActive: true,
        refetchInactive: true,
      }),
    );

    expect(mockFetcher).toHaveBeenCalledTimes(5);
    expect(mockFetcher.mock.calls[3][0].queryKey).toEqual(key1);
    expect(mockFetcher.mock.calls[4][0].queryKey).toEqual(key2);
  });

  test('trying to clear not existing tag', async () => {
    const mockFetcher = jest.fn((_: { queryKey: any[] }) => Promise.resolve({ anyField: true }));
    queryCache = new QueryCache();
    queryClient = new QueryClient({
      defaultOptions: {
        queries: {
          queryFn: mockFetcher,
        },
      },
      queryCache,
    });

    const wrapper: WrapperComponent<any> = ({ children }) => {
      return <QueryClientProvider client={queryClient}> {children} </QueryClientProvider>;
    };

    const key1 = ['api/url', { param: 1 }, ['tag1']];
    const { result: data1, waitFor } = renderHook(() => useQuery(key1), { wrapper });

    await waitFor(() => !!data1.current.data);

    const key2 = ['api/url2', { param: 2 }, ['tag1', 'tag2']];
    const { result: data2 } = renderHook(() => useQuery(key2), { wrapper });

    await waitFor(() => !!data2.current.data);

    const key3 = ['api/url3', { param: 3 }, ['tag2']];
    const { result: data3 } = renderHook(() => useQuery(key3), {
      wrapper,
    });

    await waitFor(() => !!data3.current.data);

    expect(data1.current.data).toEqual({ anyField: true });
    expect(data2.current.data).toEqual({ anyField: true });
    expect(data3.current.data).toEqual({ anyField: true });
    expect(mockFetcher).toHaveBeenCalled();
    expect(mockFetcher).toHaveBeenCalledTimes(3);
    expect(mockFetcher.mock.calls[0][0].queryKey).toEqual(key1);
    expect(mockFetcher.mock.calls[1][0].queryKey).toEqual(key2);
    expect(mockFetcher.mock.calls[2][0].queryKey).toEqual(key3);

    const { result } = renderHook(() => useQueryInvalidate(), { wrapper });

    await act(() =>
      result.current(['tag3'], {
        refetchActive: true,
        refetchInactive: true,
      }),
    );

    expect(mockFetcher).toHaveBeenCalledTimes(3);
  });
});

describe('useMutationWithQueryInvalidate', () => {
  let queryCache: QueryCache;
  let queryClient: QueryClient;
  let mockFetcher: jest.Mock;

  beforeEach(() => {
    mockFetcher = jest.fn(() => Promise.resolve({ anyField: true }));
    queryCache = new QueryCache();
    queryClient = new QueryClient({
      defaultOptions: {
        queries: {
          queryFn: mockFetcher,
        },
      },
      queryCache,
    });
  });

  test('clear tag when request has positive result', async () => {
    const mockFetcher = jest.fn((_: { queryKey: any[] }) => Promise.resolve({ anyField: true }));
    queryCache = new QueryCache();
    queryClient = new QueryClient({
      defaultOptions: {
        queries: {
          queryFn: mockFetcher,
        },
      },
      queryCache,
    });

    const wrapper: WrapperComponent<any> = ({ children }) => {
      return <QueryClientProvider client={queryClient}> {children} </QueryClientProvider>;
    };

    const key1 = ['api/url', { param: 1 }, ['tag1']];
    const { result: data1, waitFor } = renderHook(() => useQuery(key1), { wrapper });

    await waitFor(() => !!data1.current.data);

    const key2 = ['api/url2', { param: 2 }, ['tag1', 'tag2']];
    const { result: data2 } = renderHook(() => useQuery(key2), { wrapper });

    await waitFor(() => !!data2.current.data);

    const key3 = ['api/url3', { param: 3 }, ['tag2']];
    const { result: data3 } = renderHook(() => useQuery(key3), {
      wrapper,
    });

    await waitFor(() => !!data3.current.data);

    expect(mockFetcher).toHaveBeenCalled();
    expect(mockFetcher).toHaveBeenCalledTimes(3);
    expect(mockFetcher.mock.calls[0][0].queryKey).toEqual(key1);
    expect(mockFetcher.mock.calls[1][0].queryKey).toEqual(key2);
    expect(mockFetcher.mock.calls[2][0].queryKey).toEqual(key3);

    const { result } = renderHook(
      () =>
        useMutationWithQueryInvalidate(() => Promise.resolve(true), {
          invalidateEntities: ['tag1'],
        }),
      { wrapper },
    );

    await act(async () => {
      await result.current.mutateAsync();
    });

    await waitFor(() => !!result.current.data);

    expect(mockFetcher).toHaveBeenCalledTimes(5);
    expect(mockFetcher.mock.calls[3][0].queryKey).toEqual(key1);
    expect(mockFetcher.mock.calls[4][0].queryKey).toEqual(key2);
  });

  test("don't clear tag when request has negative result", async () => {
    const mockFetcher = jest.fn((_: { queryKey: any[] }) => Promise.resolve({ anyField: true }));
    queryCache = new QueryCache();
    queryClient = new QueryClient({
      defaultOptions: {
        queries: {
          queryFn: mockFetcher,
        },
      },
      queryCache,
    });

    const wrapper: WrapperComponent<any> = ({ children }) => {
      return <QueryClientProvider client={queryClient}> {children} </QueryClientProvider>;
    };

    const key1 = ['api/url', { param: 1 }, ['tag1']];
    const { result: data1, waitFor } = renderHook(() => useQuery(key1), { wrapper });

    await waitFor(() => !!data1.current.data);

    const key2 = ['api/url2', { param: 2 }, ['tag1', 'tag2']];
    const { result: data2 } = renderHook(() => useQuery(key2), { wrapper });

    await waitFor(() => !!data2.current.data);

    const key3 = ['api/url3', { param: 3 }, ['tag2']];
    const { result: data3 } = renderHook(() => useQuery(key3), {
      wrapper,
    });

    await waitFor(() => !!data3.current.data);

    expect(mockFetcher).toHaveBeenCalled();
    expect(mockFetcher).toHaveBeenCalledTimes(3);
    expect(mockFetcher.mock.calls[0][0].queryKey).toEqual(key1);
    expect(mockFetcher.mock.calls[1][0].queryKey).toEqual(key2);
    expect(mockFetcher.mock.calls[2][0].queryKey).toEqual(key3);

    const { result } = renderHook(
      () =>
        useMutationWithQueryInvalidate(() => Promise.reject(false), {
          invalidateEntities: ['tag1'],
        }),
      { wrapper },
    );

    try {
      await act(() => result.current.mutateAsync());
    } catch (e) {
      expect(mockFetcher).toHaveBeenCalledTimes(3);
    }
  });

  test('call onSuccess and clear tags', async () => {
    const onSuccess = jest.fn();
    const mockFetcher = jest.fn((_: { queryKey: any[] }) => Promise.resolve({ anyField: true }));
    queryCache = new QueryCache();
    queryClient = new QueryClient({
      defaultOptions: {
        queries: {
          queryFn: mockFetcher,
        },
      },
      queryCache,
    });

    const wrapper: WrapperComponent<any> = ({ children }) => {
      return <QueryClientProvider client={queryClient}> {children} </QueryClientProvider>;
    };

    const key1 = ['api/url', { param: 1 }, ['tag1']];
    const { result: data1, waitFor } = renderHook(() => useQuery(key1), { wrapper });

    await waitFor(() => !!data1.current.data);

    const key2 = ['api/url2', { param: 2 }, ['tag1', 'tag2']];
    const { result: data2 } = renderHook(() => useQuery(key2), { wrapper });

    await waitFor(() => !!data2.current.data);

    const key3 = ['api/url3', { param: 3 }, ['tag2']];
    const { result: data3 } = renderHook(() => useQuery(key3), {
      wrapper,
    });

    await waitFor(() => !!data3.current.data);

    expect(mockFetcher).toHaveBeenCalled();
    expect(mockFetcher).toHaveBeenCalledTimes(3);
    expect(mockFetcher.mock.calls[0][0].queryKey).toEqual(key1);
    expect(mockFetcher.mock.calls[1][0].queryKey).toEqual(key2);
    expect(mockFetcher.mock.calls[2][0].queryKey).toEqual(key3);

    const { result } = renderHook(
      () =>
        useMutationWithQueryInvalidate(() => Promise.resolve(true), {
          invalidateEntities: ['tag1'],
          onSuccess,
        }),
      { wrapper },
    );

    await act(async () => {
      await result.current.mutateAsync({});
    });

    await waitFor(() => !!result.current.data);

    expect(onSuccess).toHaveBeenCalledTimes(1);

    expect(mockFetcher).toHaveBeenCalledTimes(5);
    expect(mockFetcher.mock.calls[3][0].queryKey).toEqual(key1);
    expect(mockFetcher.mock.calls[4][0].queryKey).toEqual(key2);
  });
});
