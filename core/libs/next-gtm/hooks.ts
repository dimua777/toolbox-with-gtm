import { useEffect } from 'react';
import { useRouter } from 'next/router';

export const usePageChangeCallback = (callback: (url: string) => void) => {
  const { events } = useRouter();

  useEffect(() => {
    events.on('routeChangeComplete', callback);
    return () => {
      events.off('routeChangeComplete', callback);
    };
  }, [events, callback]);
};
