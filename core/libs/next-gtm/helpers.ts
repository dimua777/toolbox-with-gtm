import { DataLayerObject } from './types';

export const pushToDataLayer = (props: DataLayerObject) => {
  (window as any).dataLayer = (window as any).dataLayer || [];
  (window as any).dataLayer.push(props);
};

export const pageView = (url: string) => {
  pushToDataLayer({ event: 'pageview', page: url });
};

export const pushCustomEventToDataLayer = (event: Record<string, any>) =>
  pushToDataLayer(event as any);
