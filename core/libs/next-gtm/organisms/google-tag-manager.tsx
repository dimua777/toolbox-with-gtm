import { FC } from 'react';
import Script from 'next/script';

import { pageView } from '../helpers';
import { usePageChangeCallback } from '../hooks';

export const GoogleTagManager: FC<{
  gtmID: string;
}> = ({ children, gtmID }) => {
  usePageChangeCallback(pageView);

  return (
    <>
      <Script
        dangerouslySetInnerHTML={{
          __html: `
              (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
              })(window,document,'script','dataLayer', '${gtmID}');
              `,
        }}
        strategy="afterInteractive"
      />
      <noscript>
        <iframe
          height="0"
          src={`https://www.googletagmanager.com/ns.html?id=${gtmID}`}
          style={{ display: 'none', visibility: 'hidden' }}
          title="gtm"
          width="0"
        />
      </noscript>
      {children}
    </>
  );
};
