/* eslint-disable max-lines */
/* eslint-disable @typescript-eslint/naming-convention */

type EcommerceItem = {
  item_id: string;
  item_name: string;
  affiliation?: string;
  coupon?: string;
  /**
   * @param currency https://support.google.com/analytics/answer/9796179?hl=en&ref_topic=11151952#zippy=%2Cin-this-article
   */
  currency?: string;
  creative_name?: string;
  creative_slot?: string;
  discount?: number;
  index?: number;
  item_brand?: string;
  item_category?: string;
  item_category2?: string;
  item_category3?: string;
  item_category4?: string;
  item_category5?: string;
  item_list_id?: string;
  item_list_name?: string;
  item_variant?: string;
  location_id?: string;
  price?: number;
  promotion_id?: string;
  promotion_name?: string;
  quantity?: number;
};

type PageView = {
  event: 'pageview';
  page: string;
  page_location?: string;
  page_referrer?: string;
  page_title?: string;
  send_page_view?: boolean;
};

type AddPaymentInfo = {
  event: 'add_payment_info';
  currency: string;
  value: number;
  coupon?: string;
  payment_type?: string;
  items: EcommerceItem[];
};

type AddShippingInfo = {
  event: 'add_shipping_info';
  currency: string;
  value: number;
  coupon?: string;
  shipping_tier?: string;
  items: EcommerceItem[];
};

type AddToCart = {
  event: 'add_to_cart';
  currency: string;
  value: number;
  items: EcommerceItem[];
};

type AddToWishlist = {
  event: 'add_to_wishlist';
  currency: string;
  value: number;
  items: EcommerceItem[];
};

type BeginCheckout = {
  event: 'add_to_wishlist';
  currency: string;
  value: number;
  coupon?: string;
  items: EcommerceItem[];
};

type EarnVirtualCurrency = {
  event: 'earn_virtual_currency';
  virtual_currency_name?: string;
  value?: number;
};

type GenerateLead = {
  event: 'generate_lead';
  currency: string;
  value: number;
};

type JoinGroup = {
  event: 'join_group';
  group_id?: string;
};

type LevelEnd = {
  event: 'level_end';
  level_name?: string;
  success?: boolean;
};

type LevelStart = {
  event: 'level_start';
  level_name?: string;
};

type LevelUp = {
  event: 'level_up';
  level?: number;
  character?: string;
};

type Login = {
  event: 'login';
  method?: string;
};

type PostScore = {
  event: 'post_score';
  score: number;
  level?: number;
  character?: string;
};

type Purchase = {
  event: 'purchase';
  currency: string;
  transaction_id: string;
  value: number;
  affiliation?: string;
  coupon?: string;
  shipping?: string;
  tax?: number;
  items: EcommerceItem[];
};

type Refund = {
  event: 'refund';
  currency: string;
  transaction_id: string;
  value: number;
  affiliation?: string;
  coupon?: string;
  shipping?: string;
  tax?: number;
  items?: EcommerceItem[];
};

type RemoveFromCart = {
  event: 'remove_from_cart';
  currency: string;
  value: number;
  items: EcommerceItem[];
};

type Search = {
  event: 'search';
  search_term: string;
};

type SelectContent = {
  event: 'select_content';
  content_type?: string;
  item_id?: string;
};

type SelectItem = {
  event: 'select_item';
  item_list_id?: string;
  item_list_name?: string;
  items: EcommerceItem[];
};

type SelectPromotion = {
  event: 'select_promotion';
  creative_name?: string;
  creative_slot?: string;
  location_id?: string;
  promotion_id?: string;
  promotion_name?: string;
  items?: EcommerceItem[];
};

type Share = {
  event: 'share';
  method?: string;
  content_type?: string;
  item_id?: string;
};

type SignUp = {
  event: 'sign_up';
  method?: string;
};

type SpendVirtualCurrency = {
  event: 'spend_virtual_currency';
  value: number;
  virtual_currency_name: string;
  item_name?: string;
};

type TutorialBegin = {
  event: 'tutorial_begin';
};

type TutorialComplete = {
  event: 'tutorial_complete';
};

type UnlockAchievement = {
  event: 'unlock_achievement';
  achievement_id: string;
};

type ViewCart = {
  event: 'view_cart';
  currency: string;
  value: number;
  items: EcommerceItem[];
};

type ViewItem = {
  event: 'view_item';
  currency: string;
  value: number;
  items: EcommerceItem[];
};

type ViewItemList = {
  event: 'view_item_list';
  item_list_id?: string;
  item_list_name?: string;
  items: EcommerceItem[];
};

type ViewPromotion = {
  event: 'view_promotion';
  creative_name?: string;
  creative_slot?: string;
  location_id?: string;
  promotion_id?: string;
  promotion_name?: string;
  items: EcommerceItem[];
};

export type DataLayerObject =
  | PageView
  | AddPaymentInfo
  | AddShippingInfo
  | AddToCart
  | AddToWishlist
  | BeginCheckout
  | EarnVirtualCurrency
  | GenerateLead
  | JoinGroup
  | LevelEnd
  | LevelStart
  | LevelUp
  | Login
  | PostScore
  | Purchase
  | Refund
  | RemoveFromCart
  | Search
  | SelectContent
  | SelectItem
  | SelectPromotion
  | Share
  | SignUp
  | SpendVirtualCurrency
  | TutorialBegin
  | TutorialComplete
  | UnlockAchievement
  | ViewCart
  | ViewItem
  | ViewItemList
  | ViewPromotion;
