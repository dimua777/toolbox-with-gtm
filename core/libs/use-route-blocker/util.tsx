import { Collection } from './types';

export const CollectionTools = {
  remove(key: string, collection: Collection) {
    if (collection[key]) {
      const newCollection = { ...collection };
      delete newCollection[key];
      return newCollection;
    }
    return collection;
  },
  add(key: string, data: string[], collection: Collection) {
    return { ...collection, [key]: data };
  },
  getAll(collection: Collection) {
    return Object.keys(collection).reduce<string[]>((result, currentKey) => {
      return [...result, ...collection[currentKey]];
    }, []);
  },
};
