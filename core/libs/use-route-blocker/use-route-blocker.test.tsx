/* eslint-disable */
import { act, renderHook, WrapperComponent } from '@testing-library/react-hooks';
import Router, { useRouter } from 'next/router';
import { RouteBlockerProvider } from './context';
import { useRouteBlocker } from './use-route-blocker';

jest.mock('next/router', () => {
  return {
    router: {
      change: jest.fn(),
    },
    useRouter: () => {
      return { asPath: '/router/url' };
    },
  };
});


// It doesn't work.
// It requires more time for implementation.
// But I don't sure that it is necessary for us because we already have e2e tests
xdescribe('useRouteBlocker', () => {
  beforeEach(() => {
    Router.router.change.mockReset();
  });

  test('disable state', async () => {
    const handlerOfBlocker = jest.fn((cb: any) => {
      cb(true);
    });
    const wrapper: WrapperComponent<any> = ({ children }) => (
      <RouteBlockerProvider handlerOfBlocker={handlerOfBlocker}>{children}</RouteBlockerProvider>
    );

    renderHook(
      () =>
        useRouteBlocker(false, {
          allowedUrls: [],
        }),
      {
        wrapper,
      },
    );

    expect(handlerOfBlocker).not.toHaveBeenCalled();

    await Router.router.change('/any/url', '/any/url');

    expect(handlerOfBlocker).not.toHaveBeenCalled();
  });

  test('state is enabled with default arguments', async () => {
    const handlerOfBlocker = jest.fn((cb: any) => {
      cb(true);
    });
    const wrapper: WrapperComponent<any> = ({ children }) => (
      <RouteBlockerProvider handlerOfBlocker={handlerOfBlocker}>{children}</RouteBlockerProvider>
    );

    renderHook(
      () =>
        useRouteBlocker(true, {
          allowedUrls: [],
        }),
      { wrapper },
    );

    expect(handlerOfBlocker).not.toHaveBeenCalled();

    await Router.router.change('/any/url', '/any/url');

    expect(handlerOfBlocker).toHaveBeenCalled();
  });

  test('with custom useInjectInRouterHook', async () => {
    const handlerOfBlocker = jest.fn((cb: any) => {
      cb(true);
    });
    const useInjectInRouterHook = jest.fn();
    const wrapper: WrapperComponent<any> = ({ children }) => (
      <RouteBlockerProvider
        useInjectInRouterHook={useInjectInRouterHook}
        handlerOfBlocker={handlerOfBlocker}
      >
        {children}
      </RouteBlockerProvider>
    );

    renderHook(
      () =>
        useRouteBlocker(true, {
          allowedUrls: [],
        }),
      { wrapper },
    );

    expect(useInjectInRouterHook).toHaveBeenCalled();
    expect(useInjectInRouterHook).toHaveBeenCalledTimes(2);
    expect(useInjectInRouterHook).toHaveBeenLastCalledWith({
      enable: true,
      allowedUrls: [],
      handleRouteChangeStart: handlerOfBlocker,
    });
  });

  test('with custom useBeforePopStateHook', async () => {
    const handlerOfBlocker = jest.fn((cb: any) => {
      cb(true);
    });
    const useBeforePopStateHook = jest.fn();
    const wrapper: WrapperComponent<any> = ({ children }) => (
      <RouteBlockerProvider
        useBeforePopStateHook={useBeforePopStateHook}
        handlerOfBlocker={handlerOfBlocker}
      >
        {children}
      </RouteBlockerProvider>
    );

    renderHook(
      () =>
        useRouteBlocker(true, {
          allowedUrls: [],
        }),
      { wrapper },
    );

    expect(useBeforePopStateHook).toHaveBeenCalled();
    expect(useBeforePopStateHook).toHaveBeenCalledTimes(2);
    expect(useBeforePopStateHook).toHaveBeenLastCalledWith({
      enable: true,
      allowedUrls: [],
      handleRouteChangeStart: handlerOfBlocker,
    });
  });

  test('defaultAllowedUrls in provider', async () => {
    const handlerOfBlocker = jest.fn((cb: any) => {
      cb(true);
    });
    const useInjectInRouterHook = jest.fn();
    const wrapper: WrapperComponent<any> = ({ children }) => (
      <RouteBlockerProvider
        useInjectInRouterHook={useInjectInRouterHook}
        handlerOfBlocker={handlerOfBlocker}
        defaultAllowedUrls={['/any/allowed/url']}
      >
        {children}
      </RouteBlockerProvider>
    );

    renderHook(
      () =>
        useRouteBlocker(true, {
          allowedUrls: [],
        }),
      { wrapper },
    );

    expect(useInjectInRouterHook).toHaveBeenCalled();
    expect(useInjectInRouterHook).toHaveBeenCalledTimes(2);
    expect(useInjectInRouterHook).toHaveBeenLastCalledWith({
      enable: true,
      allowedUrls: ['/any/allowed/url'],
      handleRouteChangeStart: handlerOfBlocker,
    });
  });

  test('allowedUrls in main hook', async () => {
    const handlerOfBlocker = jest.fn((cb: any) => {
      cb(true);
    });
    const useInjectInRouterHook = jest.fn();
    const wrapper: WrapperComponent<any> = ({ children }) => (
      <RouteBlockerProvider
        useInjectInRouterHook={useInjectInRouterHook}
        handlerOfBlocker={handlerOfBlocker}
      >
        {children}
      </RouteBlockerProvider>
    );

    renderHook(
      () =>
        useRouteBlocker(true, {
          allowedUrls: ['/any/allowed/url'],
        }),
      { wrapper },
    );

    expect(useInjectInRouterHook).toHaveBeenCalled();
    expect(useInjectInRouterHook).toHaveBeenCalledTimes(2);
    expect(useInjectInRouterHook).toHaveBeenLastCalledWith({
      enable: true,
      allowedUrls: ['/any/allowed/url'],
      handleRouteChangeStart: handlerOfBlocker,
    });
  });

  test('allowedUrls with special hook', async () => {
    const handlerOfBlocker = jest.fn((cb: any) => {
      cb(true);
    });
    const useInjectInRouterHook = jest.fn();
    const wrapper: WrapperComponent<any> = ({ children }) => (
      <RouteBlockerProvider
        useInjectInRouterHook={useInjectInRouterHook}
        handlerOfBlocker={handlerOfBlocker}
      >
        {children}
      </RouteBlockerProvider>
    );

    renderHook(
      () =>
        useRouteBlocker(true, {
          allowedUrls: ['/any/allowed/url'],
        }),
      { wrapper },
    );

    expect(useInjectInRouterHook).toHaveBeenCalled();
    expect(useInjectInRouterHook).toHaveBeenCalledTimes(2);
    expect(useInjectInRouterHook).toHaveBeenLastCalledWith({
      enable: true,
      allowedUrls: ['/any/allowed/url'],
      handleRouteChangeStart: handlerOfBlocker,
    });
  });
});
