import { useEffect, useRef } from 'react';
import Router from 'next/router';

import { BlockerHooksProps } from './types';

// TODO: hack next/router because we can't stop redirecting to another route
// fix me please if team of nextjs will add such functionality
export const useMonkeyPatchingRouter = ({
  enable,
  allowedUrls,
  handleRouteChangeStart,
}: BlockerHooksProps) => {
  // Keep dynamic data for static hook
  const allowedUrlsRef = useRef<typeof allowedUrls>(allowedUrls);
  allowedUrlsRef.current = allowedUrls;

  const handleRouteChangeStartRef = useRef<typeof handleRouteChangeStart>(handleRouteChangeStart);
  handleRouteChangeStartRef.current = handleRouteChangeStart;

  useEffect(() => {
    let _original: any;

    if (Router.router && enable) {
      _original = (Router.router as any).change;
      (Router.router as any).change = async (...args: any[]) => {
        return new Promise<boolean>(resolve => {
          // These conditions are needed to check url is allowed
          // however, original function `change` (Router.router.change) has signature:
          //   change(method: HistoryMethod, url: string, as: string, options: TransitionOptions): Promise<boolean>;
          // so we need to check second and third arguments on including into skipping routers
          if (
            allowedUrlsRef.current.includes(args[1]) ||
            allowedUrlsRef.current.includes(args[2])
          ) {
            resolve(true);
          } else {
            handleRouteChangeStartRef.current(resolve);
          }
        }).then(res => {
          if (res) {
            return _original.apply(Router.router, args);
          }
          return false;
        });
      };
    }

    return () => {
      if (Router.router && _original) {
        (Router.router as any).change = _original;
      }
    };
  }, [enable]);
};
