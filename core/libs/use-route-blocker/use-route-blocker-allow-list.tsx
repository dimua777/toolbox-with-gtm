import { useEffect } from 'react';

import { useCommonAllowedUrls } from './context';

export const useRouteBlockerAllowList = (allowList: string[]) => {
  const setCommonAllowedUrls = useCommonAllowedUrls();
  useEffect(() => {
    setCommonAllowedUrls(allowList);
    return () => {
      setCommonAllowedUrls([]);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, allowList);
};
