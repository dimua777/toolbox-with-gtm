import { useEffect, useRef } from 'react';
import { nanoid } from 'nanoid';

import { useRouteBlockerConsumer } from './context';

const useGetUniqueId = () => {
  const uniqueIdRef = useRef('');
  if (!uniqueIdRef.current) {
    uniqueIdRef.current = nanoid();
  }

  return uniqueIdRef.current;
};

export const useRouteBlocker = (isEnabled = false, options?: { allowedUrls?: string[] }) => {
  const uniqueId = useGetUniqueId();
  const consumer = useRouteBlockerConsumer();

  const allowedUrls = options?.allowedUrls ?? [];

  useEffect(() => {
    consumer(isEnabled, { allowedUrls, uniqueId });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [...allowedUrls, isEnabled, uniqueId]);

  // Unsubscribe after hook was unmounted
  // We must make sure that blocker state would be false after a routing was changed
  useEffect(() => {
    return () => consumer(false, { uniqueId });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};
