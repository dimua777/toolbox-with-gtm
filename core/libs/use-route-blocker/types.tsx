export type HandleRouteChangeStartCallback = (cb: (continueRedirect: boolean) => void) => void;

export interface BlockerHooksProps {
  enable: boolean;
  allowedUrls: string[];
  handleRouteChangeStart: HandleRouteChangeStartCallback;
}

export type Collection = { [key: string]: string[] };
