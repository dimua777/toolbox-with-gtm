/* eslint-disable */
import { renderHook } from '@testing-library/react-hooks';
import Router from 'next/router';
import { useMonkeyPatchingRouter } from './use-monkey-patching-router';

jest.mock('next/router', () => {
  return {
    router: {
      change: jest.fn(),
    },
  };
});

describe('useMonkeyPatchingRouter', () => {
  beforeEach(() => {
    Router.router.change.mockReset();
  });

  test('disable state', async () => {
    const handleRouteChangeStart = jest.fn();
    renderHook(() =>
      useMonkeyPatchingRouter({
        enable: false,
        allowedUrls: [],
        handleRouteChangeStart,
      }),
    );

    expect(Router.router.change).not.toHaveBeenCalled();
    expect(handleRouteChangeStart).not.toHaveBeenCalled();

    await Router.router.change();

    expect(Router.router.change).toHaveBeenCalled();
    expect(handleRouteChangeStart).not.toHaveBeenCalled();
  });

  describe('state is enabled', () => {
    test('handler allows redirect to another route', async () => {
      const handleRouteChangeStart = jest.fn(cb => {
        cb(true);
      });
      const original = Router.router.change;

      renderHook(() =>
        useMonkeyPatchingRouter({
          enable: true,
          allowedUrls: [],
          handleRouteChangeStart,
        }),
      );

      expect(original).not.toHaveBeenCalled();
      expect(handleRouteChangeStart).not.toHaveBeenCalled();
      expect(original !== Router.router.change).toBeTruthy();

      await Router.router.change('/any/url', '/any/url');

      expect(original).toHaveBeenCalled();
      expect(original).toHaveBeenLastCalledWith('/any/url', '/any/url');
      expect(handleRouteChangeStart).toHaveBeenCalled();
    });

    test("handler doesn't allow redirect to another route", async () => {
      const handleRouteChangeStart = jest.fn(cb => {
        cb(false);
      });
      const original = Router.router.change;

      renderHook(() =>
        useMonkeyPatchingRouter({
          enable: true,
          allowedUrls: [],
          handleRouteChangeStart,
        }),
      );

      expect(original).not.toHaveBeenCalled();
      expect(handleRouteChangeStart).not.toHaveBeenCalled();
      expect(original !== Router.router.change).toBeTruthy();

      await Router.router.change('/any/url', '/any/url');

      expect(original).not.toHaveBeenCalled();
      expect(handleRouteChangeStart).toHaveBeenCalled();
    });

    describe('allowed urls', () => {
      test('testing url is included into allowed url list', async () => {
        const handleRouteChangeStart = jest.fn(cb => {
          cb(true);
        });
        const original = Router.router.change;

        renderHook(() =>
          useMonkeyPatchingRouter({
            enable: true,
            allowedUrls: ['/any/url'],
            handleRouteChangeStart,
          }),
        );

        expect(original).not.toHaveBeenCalled();
        expect(handleRouteChangeStart).not.toHaveBeenCalled();
        expect(original !== Router.router.change).toBeTruthy();

        await Router.router.change('/any/url', '/any/url');

        expect(original).toHaveBeenCalled();
        expect(original).toHaveBeenLastCalledWith('/any/url', '/any/url');
        expect(handleRouteChangeStart).not.toHaveBeenCalled();
      });

      test('testing url is not included into allowed url list', async () => {
        const handleRouteChangeStart = jest.fn(cb => {
          cb(true);
        });
        const original = Router.router.change;

        renderHook(() =>
          useMonkeyPatchingRouter({
            enable: true,
            allowedUrls: ['/any/url2'],
            handleRouteChangeStart,
          }),
        );

        expect(original).not.toHaveBeenCalled();
        expect(handleRouteChangeStart).not.toHaveBeenCalled();
        expect(original !== Router.router.change).toBeTruthy();

        await Router.router.change('/any/url', '/any/url');

        expect(original).toHaveBeenCalled();
        expect(original).toHaveBeenLastCalledWith('/any/url', '/any/url');
        expect(handleRouteChangeStart).toHaveBeenCalled();
      });
    });
  });
});
