import { useEffect, useRef } from 'react';
import Router, { useRouter } from 'next/router';

import { BlockerHooksProps } from './types';

// This is an interception of changing popstate of the window
// It doesn't allow an user to leave blocked page with browsers arrows (left and right by history)
// We are intercepting these requests and forbid them
export const useDefaultBeforePopState = ({ enable, allowedUrls }: BlockerHooksProps) => {
  const router = useRouter();

  const allowedUrlsRef = useRef<typeof allowedUrls>(allowedUrls);
  allowedUrlsRef.current = allowedUrls;

  useEffect(() => {
    Router.beforePopState(({ as, url }) => {
      if (allowedUrlsRef.current.includes(as)) {
        return true;
      }

      if (enable) {
        window.history.pushState(
          {
            as: router.asPath,
            url: router.asPath,
          },
          '',
          router.asPath,
        );
        return false;
      }

      return true;
    });
  }, [router.asPath, enable]);
};
