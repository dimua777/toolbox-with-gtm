import { Reducer, useCallback, useReducer } from 'react';
import constate from 'constate';

import { BlockerHooksProps, Collection, HandleRouteChangeStartCallback } from './types';
import { useDefaultBeforePopState } from './use-default-before-pop-state';
import { useMonkeyPatchingRouter } from './use-monkey-patching-router';
import { CollectionTools } from './util';

interface EnableBlockerAction {
  type: 'enable';
}

interface IDisableBlockerAction {
  type: 'disable';
}

interface SetCommonAllowedUrlsAction {
  type: 'common-allowed-urls';
  payload: { allowedUrls: string[] };
}

interface SetCurrentHookAllowedUrlsAction {
  type: 'current-hook-allowed-urls';
  payload: { allowedUrls: string[]; uniqueId: string };
}

type Actions =
  | EnableBlockerAction
  | IDisableBlockerAction
  | SetCommonAllowedUrlsAction
  | SetCurrentHookAllowedUrlsAction;

interface State {
  amountBlockedCallings: number;
  allowedUrls: {
    default: string[];
    allowedList: string[];
    hooks: Collection;
    all: string[];
  };
}

const reducer: Reducer<State, Actions> = (prevState, action) => {
  switch (action.type) {
    case 'enable': {
      return {
        ...prevState,
        amountBlockedCallings: prevState.amountBlockedCallings + 1,
      };
    }

    case 'disable': {
      const amountBlockedCallings = prevState.amountBlockedCallings - 1;
      return {
        ...prevState,
        amountBlockedCallings: amountBlockedCallings > 0 ? amountBlockedCallings : 0,
      };
    }

    case 'common-allowed-urls': {
      const commonAllowedUrls = action.payload.allowedUrls;
      return {
        ...prevState,
        allowedUrls: {
          ...prevState.allowedUrls,
          allowedList: commonAllowedUrls,
          all: [
            ...prevState.allowedUrls.default,
            ...commonAllowedUrls,
            ...CollectionTools.getAll(prevState.allowedUrls.hooks),
          ],
        },
      };
    }

    case 'current-hook-allowed-urls': {
      const currentHookAllowedUrls = action.payload.allowedUrls;
      const currentHookUniqueId = action.payload.uniqueId;
      const hooksAllowedUrls =
        currentHookAllowedUrls.length === 0
          ? CollectionTools.remove(currentHookUniqueId, prevState.allowedUrls.hooks)
          : CollectionTools.add(
              currentHookUniqueId,
              currentHookAllowedUrls,
              prevState.allowedUrls.hooks,
            );

      return {
        ...prevState,
        allowedUrls: {
          ...prevState.allowedUrls,
          hooks: hooksAllowedUrls,
          all: [
            ...prevState.allowedUrls.default,
            ...prevState.allowedUrls.allowedList,
            ...CollectionTools.getAll(hooksAllowedUrls),
          ],
        },
      };
    }

    default:
      return prevState;
  }
};

interface Props {
  defaultAllowedUrls?: string[];
  handlerOfBlocker: HandleRouteChangeStartCallback;
  useInjectInRouterHook?: (props: BlockerHooksProps) => void;
  useBeforePopStateHook?: (props: BlockerHooksProps) => void;
}

const useRouteBlockerContext = ({
  defaultAllowedUrls = [],
  handlerOfBlocker,
  useInjectInRouterHook = useMonkeyPatchingRouter,
  useBeforePopStateHook = useDefaultBeforePopState,
}: Props) => {
  const [state, dispatch] = useReducer(reducer, {
    amountBlockedCallings: 0,
    allowedUrls: {
      default: defaultAllowedUrls,
      allowedList: [],
      hooks: {},
      all: [...defaultAllowedUrls],
    },
  });

  const enabled = state.amountBlockedCallings > 0;

  useInjectInRouterHook({
    enable: enabled,
    allowedUrls: state.allowedUrls.all,
    handleRouteChangeStart: handlerOfBlocker,
  });

  useBeforePopStateHook({
    enable: enabled,
    allowedUrls: state.allowedUrls.all,
    handleRouteChangeStart: handlerOfBlocker,
  });

  const consumer = useCallback(
    (isEnabled = false, options: { allowedUrls?: string[]; uniqueId: string }) => {
      const { allowedUrls = [], uniqueId } = options;

      dispatch({
        type: 'current-hook-allowed-urls',
        payload: { allowedUrls: isEnabled ? allowedUrls : [], uniqueId },
      });

      if (isEnabled) {
        dispatch({
          type: 'enable',
        });
      } else {
        dispatch({
          type: 'disable',
        });
      }
    },
    [],
  );

  const setCommonAllowedUrls = useCallback((allowedUrls: string[]) => {
    dispatch({ type: 'common-allowed-urls', payload: { allowedUrls } });
  }, []);

  return { consumer, setCommonAllowedUrls };
};

export const [RouteBlockerProvider, useRouteBlockerConsumer, useCommonAllowedUrls] = constate(
  useRouteBlockerContext,
  ({ consumer }) => consumer,
  ({ setCommonAllowedUrls }) => setCommonAllowedUrls,
);
