import { FORM_ERROR, setIn } from 'final-form';

import { ObjectSchema, ValidationError } from '../yup';

export const validateFormValues =
  <T>(schema: ObjectSchema) =>
  (values: T) => {
    try {
      schema.validateSync(values, { abortEarly: false });
    } catch (err) {
      const errors = (err as ValidationError).inner.reduce(
        (formError: Record<string, string>, innerError: ValidationError) => {
          return setIn(formError, innerError.path, innerError.message);
        },
        {},
      );

      return errors;
    }
  };

// FIXME: finalize for your project needs
export const handleMutationErrors = ({ errors, error }: APIError) => {
  if (Object.keys(errors).length > 0) {
    return errors;
  }

  return { [FORM_ERROR]: error };
};

export const defaultSubscription = {
  dirty: true,
  submitError: true,
  submitSucceeded: true,
  submitting: true,
};
