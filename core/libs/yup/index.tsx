import { setLocale } from 'yup';

// full locale: https://github.com/jquense/yup/blob/master/src/locale.ts
const validationLocale = {
  number: {
    min: '${path} must be greater than or equal to ${min}',
    max: '${path} must be less than or equal to ${max}',
  },
  string: {
    min: '${path} must be at least ${min} characters',
    max: '${path} must be at most ${max} characters',
    email: '${path} must be a valid email',
    url: '${path} must be a valid URL',
    trim: '${path} must be a trimmed string',
  },
  mixed: {
    required: '${path} is a required field',
    oneOf: '${path} must be one of the following values: ${values}',
  },
  date: {
    required: '${path} is a required field',
  },
};

setLocale(validationLocale as any);

export * from 'yup';
