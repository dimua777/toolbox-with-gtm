type PageError = { statusCode: number } & Error;

export const emitPageError = (error: PageError = new Error() as PageError, statusCode = 500) => {
  error.statusCode = error.statusCode || statusCode;
  throw error;
};
