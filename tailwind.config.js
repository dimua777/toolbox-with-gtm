/* eslint-disable @typescript-eslint/no-var-requires */
module.exports = {
  // Storybook only config
  purge: ['./app/(features|pages)/**/*.{ts,tsx,mdx}', './(core|ui|docs)/**/*.{ts,tsx,mdx}'],
  presets: [require('./ui/tailwind.config')],
};
