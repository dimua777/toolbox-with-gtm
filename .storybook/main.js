module.exports = {
  "stories": [
    "../docs/**/*.stories.mdx",
    "../app/**/*.stories.mdx",
    "../core/**/*.stories.mdx",
    "../ui/theme/*.stories.mdx",
    "../ui/**/*.stories.mdx",
  ],
  "addons": [
    "@storybook/addon-essentials",
    "@storybook/addon-links",
    {
      name: '@storybook/addon-postcss',
      options: {
        postcssLoaderOptions: {
          implementation: require('postcss'),
        },
      },
    }
  ],
  // FIXME: uncomment when webpack5 will be stable https://github.com/storybookjs/storybook/issues/14118
  // "core": {
  //   "builder": "webpack5"
  // },
  webpackFinal: async (config, { configType }) => {
    config.module.rules.push({
      test: /\.tsx$/,
      use: [
        {
          loader: 'astroturf/loader',
          options: { extension: '.module.css' },
        },
      ],
    });

    // css-modules support
    // https://github.com/storybookjs/storybook/pull/12837
    const cssRulesIndex = config.module.rules.findIndex(rule => rule.test.toString() === '/\\.css$/');
    config.module.rules[cssRulesIndex].use[1] = {
      loader: require.resolve('css-loader'),
      options: {
        importLoaders: 1,
        modules: { auto: true },
      },
    };

    return config
  }
}
