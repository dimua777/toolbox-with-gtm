import { FC } from 'react';

import { UIProvider } from '../ui'
import '../ui/organisms/global-styles/index.css';

export const StoryWrapper: FC = ({ children }) => {
  return (
    <UIProvider>
      <div className="font-font1">
        {children}
      </div>
    </UIProvider>
  )
}