// eslint-disable-next-line @typescript-eslint/no-var-requires

module.exports = {
  verbose: true,
  moduleFileExtensions: ['js', 'jsx', 'ts', 'tsx'],
  testPathIgnorePatterns: ['/node_modules/', '/.next/'],
  transformIgnorePatterns: ['/node_modules/', '!/node_modules/@sh/*'],
  projects: ['<rootDir>/*/jest.config.js'],
};
