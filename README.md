# Shakuro React toolbox ([Storybook documentation](https://shakuro-react-toolbox.vercel.app/))

[Starter UI-kit figma](https://www.figma.com/file/7NnFCu4jFGBM1EIXSWNAVX/Starterkit-2.0?node-id=795%3A0)

team driven starter for React projects.

## Recomendations

- use `git-cz`

## How to run the toolbox

- install [volta](https://volta.sh/) (if not installed)

- provide env variables: `cp app/.env.example app/.env`

## New project setup based on toolbox

- install [volta](https://volta.sh/) (if not installed)

- generate project from starter & open: `npx degit gitlab:shakurocom/react-toolbox --mode=git {projName} && cd {projName} && yarn`

- `mv example.README.md README.md`

- update `README.md` content

- search `FIXME` in files and fix it - can be something 'framework' specific etc

- remove `app/features/test` and `app/pages/test-pages`

- provide env variables: `cp app/.env.example app/.env`

- replace `#1f1f2f` with own brand color

- commit changes\*

> \* if you setup project into new repository you should:
> create empty repo on gitlab, and add remote origin `git init && git remote add origin git@gitlab.com:shakurocom/{projName}.git`

> \*\* if you setup project not into .git root (for example in `frontend` dir) - your `postinstall` script should be: `"cd .. && husky install frontend/.husky"`

> \*\*\* some useful information also available in `example.README.md`
