declare module '*.jpg';
declare module '*.jpeg';
declare module '*.png';
declare module '*.svg';
declare module '*.ico';
declare module '*.woff';
declare module '*.woff2';
declare module '*.mp4';

/**
 * for custom primitive types for example:
 *
 * // date string in HH:MM format
 * type TimeString = string;
 * type Test = { time: TimeString; };
 * const t: Test = { time: '10:20' };
 * // (property) time: string - here we loss 'TimeString'
 * t.s;
 *
 * -------------------
 * OR we can use Opaque type
 * -------------------
 *
 * // date string in HH:MM format
 * type TimeString = Opaque<string, 'TimeString'>;
 * type Test = { time: TimeString; };
 * const t: Test = { time: '10:20' };
 * // (property) time: TimeString - now we can jump into TimeString definition and se info about format
 * t.s;
 */
// eslint-disable-next-line @typescript-eslint/naming-convention
type Opaque<T, K> = T & { __opaque__: K };

type DeepPartial<T> = {
  [P in keyof T]?: DeepPartial<T[P]>;
};

type GetComponentProps<T> = React.ComponentProps<T>;

type GetAstroturfComponentProps<T> = T extends import('astroturf/react').StyledComponent<
  infer C,
  infer P,
  never
>
  ? {
      as?: keyof JSX.IntrinsicElements | React.ComponentType<any>;
      [key: string]: any;
      /**
       * if we override as with string value we can't infer props:
       * <Button as="a" href="/test" />
       * we can't infer href
       */
      // eslint-disable-next-line @typescript-eslint/ban-types
    } & (C extends keyof JSX.IntrinsicElements ? JSX.IntrinsicElements[C] : {}) &
      P
  : GetComponentProps<T>;

type BackendEntity = 'user';

type ID = string | number;

interface APIResponse<T, M = Record<string, unknown>> {
  payload: T;
  meta?: M;
}

interface APIListResponse<T, M = Record<string, unknown>> {
  payload: T[];
  meta: {
    totalPages: number;
    /**
     * TODO: deal with backend
     * it will be good return currentPage to be closer to GQL pagination
     * (all needed info for pagination encapsulated in pageInfo)
     */
    // currentPage: number;
  } & M;
}

interface APIError<T = unknown> {
  error: string;
  errors: Record<string, any>;
  payload: T;
  statusCode: number;
}

type EqualsTest<T> = <A>() => A extends T ? 1 : 0;
type Equals<A1, A2> = EqualsTest<A2> extends EqualsTest<A1> ? 1 : 0;
type Filter<K, I> = Equals<K, I> extends 1 ? never : K;
/**
 * Allows to remove index signature definition from given type
 * @typedef OmitIndex
 * @example
 *
 * type Foo = {
 *  foo: boolean;
 *  bar: string;
 *  [key: string]: any;
 * }
 *
 * type FooKeys = keyof Foo; // string | number
 *
 * type CleanFoo = OmitIndex<Foo>;
 *
 * type CleanFooKeys = keyof CleanFoo; // "foo" | "bar"
 */
type OmitIndex<T, I extends string | number = string | number> = I extends unknown
  ? {
      [K in keyof T as Filter<K, I>]: T[K];
    }
  : never;
